﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    /// <summary>
    /// Three-digit textbox only accepts values between 0-255 (dec)
    /// </summary>
    class IPTextboxItem
    {
        #region Fields
        
        // stores the previous state of the textbox
        private string previousState;

        #endregion
        
        #region Event handlers
        /// <summary>
        /// Raised when the left arrow key is pressed while the textbox is in focus
        /// </summary>
        public event EventHandler LeftArrowPressed;

        /// <summary>
        /// Raised when the right arrow key is pressed while the textbox is in focus
        /// </summary>
        public event EventHandler RightArrowPressed;

        /// <summary>
        /// Raised when the Enter key is pressed while the textbox is in focus
        /// </summary>
        public event EventHandler EnterPressed;

        #endregion

        #region Properties

        /// <summary>
        /// Three digit textbox
        /// </summary>
        public TextBox textbox
        {
            get; set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initialises a three digit textbox assigns it to the panel
        /// </summary>
        /// <param name="parentPanel">Name of the parent panel</param>
        /// <param name="position"></param>
        /// <param name="locationX">Horizontal location on the panel</param>
        /// <param name="locationY">Vertical location on the panel</param>
        public IPTextboxItem(Panel parentPanel, int position, int locationX, int locationY)
        {
            textbox = new TextBox();
            textbox.Size = UIProperties.IPTextbox_Item_Size;
            textbox.TextAlign = HorizontalAlignment.Center;
            textbox.MaxLength = 3;
            textbox.BorderStyle = BorderStyle.FixedSingle;
            textbox.Location = new Point(locationX, locationY);

            textbox.TextChanged += Textextbox_TextChanged;
            textbox.KeyDown += Textbox_KeyDown;
            textbox.GotFocus += Textbox_GotFocus;
            

            textbox.Parent = parentPanel;
            textbox.Show();
        }

        #endregion

        #region Private methods

        private string ValidateContent(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                int parse;

                if (int.TryParse(content, out parse))
                {
                    if (parse >= 0 && parse <= 255)
                    {
                        if (content.Length > 1 && content.StartsWith("0"))
                        {
                            // removes unnecessary zeros from the beginning of the text
                            content = content.Remove(0, 1);
                        }

                        previousState = content;
                        return content;
                    }
                    else
                    {
                        return previousState;
                    }
                }
                else
                {
                    return previousState;
                }
            }
            else
            {
                previousState = string.Empty;
                return string.Empty;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Gets triggered when the textbox receives focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Textbox_GotFocus(object sender, EventArgs e)
        {
            textbox.Text = ValidateContent(textbox.Text);
        }

        /// <summary>
        /// Handles special keycpresses in the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Textbox_KeyDown(object sender, KeyEventArgs e)
        {
            // moves the focus to the previous textbox if the left arrow key is pressed and the cursor is at the end if the text
            if (e.KeyCode == Keys.Left && textbox.SelectionStart == 0)
            {
                OnLeftarrowPressed();
            }

            // moves the focus to the next textbox if the right arrow key is pressed and the cursor is at the end if the text
            if (e.KeyCode == Keys.Right && textbox.SelectionStart == textbox.Text.Length)
            {
                OnRightArrowPressed();
            }

            // deletes textbox content when DEL key is pressed
            if (e.KeyCode == Keys.Delete)
            {
                textbox.Text = string.Empty;
                previousState = string.Empty;
            }

            if (e.KeyCode == Keys.Enter)
            {
                OnEnterPressed();
            }
        }

        /// <summary>
        /// Gets triggered when the content changes in the thextbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Textextbox_TextChanged(object sender, EventArgs e)
        {
            textbox.Text = ValidateContent(textbox.Text);
            textbox.SelectionStart = textbox.Text.Length;
        }

        /// <summary>
        /// Handles the left arrow pressed event
        /// </summary>
        protected virtual void OnLeftarrowPressed()
        {
            if (LeftArrowPressed != null)
            {
                LeftArrowPressed(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Handles the right arrow pressed event
        /// </summary>
        protected virtual void OnRightArrowPressed()
        {
            if (RightArrowPressed != null)
            {
                RightArrowPressed(this, EventArgs.Empty);
            }
        }

        protected virtual void OnEnterPressed()
        {
            if (EnterPressed != null)
            {
                EnterPressed(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
