﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    public class AppInfoLabel
    {
        #region Fields

        // update countdown
        private ToolTip balloon;
        private List<AppInfo> appInfo;
        private int appInfoDisplayCounter;
        private int updateInterval_Tick = UIProperties.InfoLabel_UpdateInterval_Ticks;
        private int highestErrorLevel;

        #endregion

        #region Properties

        /// <summary>
        /// Label control
        /// </summary>
        public Label Label
        {
            get; set;
        }

        /// <summary>
        /// Stores the full message string
        /// </summary>
        public string Message
        {
            get; set;
        }

        /// <summary>
        /// Collection of app messages
        /// </summary>
        public List<AppInfo> AppInfo
        {
            set
            {
                appInfo = value;
            }
        }

        /// <summary>
        /// Highest error level of the app
        /// </summary>
        public int HighestErrorLevel
        {
            get
            {
                return highestErrorLevel;
            }
        }

        #endregion

        #region Constructors

        public AppInfoLabel()
        {
            AppInfo = new List<INDEDashboard.AppInfo>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates a new instance of the info label.
        /// </summary>
        /// <param name="parent">The parent control of the new instance</param>
        /// <param name="location">The relative location on the parent control</param>
        /// <param name="font"></param>
        public Label AddLabel(Control parent, Point location, Size labelSize ,Font font)
        {
            appInfoDisplayCounter = 0;

            balloon = new ToolTip();
            Message = string.Empty;

            Label = new Label();
            Label.Parent = parent;
            Label.Location = location;
            Label.Font = font;
            Label.Size = labelSize;
            Label.TextAlign = ContentAlignment.MiddleLeft;
            Label.BackColor = UIProperties.BackColour_Default;
            Label.ForeColor = UIProperties.FontColor_Default;
            Label.Text = "---";
            Label.Show();
            Label.MouseHover += Label_MouseHover;

            return Label;
        }

        /// <summary>
        /// Adds new appinfo. An existing appinfo with the same text will be replaced with the new one
        /// </summary>
        /// <param name="newInfo">New appinfo with error message, expiry and error level</param>
        public void AddInfo(AppInfo newInfo)
        {
            bool exists = false;

            for (int i = appInfo.Count - 1; i >= 0; i--)
            {
                if (appInfo[i].ErrorMessage == newInfo.ErrorMessage)
                {
                    exists = true;

                    appInfo.RemoveAt(i);
                    appInfo.Insert(i, newInfo);
                }
            }

            if (!exists)
            {
                appInfo.Add(newInfo);
            }
        }

        /// <summary>
        /// Updates the list of appinfos, and label text and error level
        /// </summary>
        public void Update()
        {
            UpdateInfoList();

            if (Label != null)
            {
                UpdateLabelText();
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Updates the info list, removes expired entries, updates highest error level
        /// </summary>
        private void UpdateInfoList()
        {
            int checkErrorLevel = 0;

            for (int i = appInfo.Count - 1; i >= 0; i--)
            {
                if (appInfo[i].CheckExpired())
                {
                    appInfo.RemoveAt(i);
                }
                else
                {
                    if (appInfo[i].ErrorLevel > checkErrorLevel)
                    {
                        checkErrorLevel = appInfo[i].ErrorLevel;
                    }
                }
            }

            highestErrorLevel = checkErrorLevel;
        }

        /// <summary>
        /// Updates the label text and the Message property. Chops the label text if it's longer than allowed.
        /// </summary>
        private void UpdateLabelText()
        {
            if (updateInterval_Tick < 1)
            {
                updateInterval_Tick = UIProperties.InfoLabel_UpdateInterval_Ticks;

                Message = string.Empty;
    
                if (appInfo.Count > 0)
                {
                    if (appInfoDisplayCounter < 0)
                    {
                        appInfoDisplayCounter = appInfo.Count - 1;
                    }

                    switch (appInfo[appInfoDisplayCounter].ErrorLevel)
                    {
                        case 1:
                            Message = "WARNING ";
                            break;
                        case 2:
                            Message = "ERROR ";
                            break;
                        default:
                            Message = "INFO ";
                            break;
                    }

                    Message += appInfo[appInfoDisplayCounter].ErrorMessage;

                    appInfoDisplayCounter--;
                }
                else
                {
                    Message = "---";
                }

                if (Message.Length > UIProperties.InfoLabel_VisibleTextLength)
                {
                    Label.Text = Message.Remove(UIProperties.InfoLabel_VisibleTextLength) + "...";
                }
                else
                {
                    Label.Text = Message;
                }
            }

            updateInterval_Tick--;
        }

        #endregion

        #region Events

        /// <summary>
        /// Displays a tooltip balloon if the message is long
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label_MouseHover(object sender, EventArgs e)
        {
            if (Message.Length > UIProperties.InfoLabel_VisibleTextLength || appInfo.Count > 1)
            {
                balloon.IsBalloon = false;
                balloon.UseFading = true;
                balloon.BackColor = Color.Aquamarine;
                if (appInfo.Count > 1)
                {
                    Message = "application messages";

                    foreach (var item in appInfo)
                    {
                        Message += "\r\n";

                        switch (item.ErrorLevel)
                        {
                            case 1:
                                Message += "WARNING ";
                                break;
                            case 2:
                                Message += "ERROR ";
                                break;
                            default:
                                Message += "INFO ";
                                break;
                        }

                        Message += item.ErrorMessage;
                    }
                }

                balloon.Show(Message, this.Label);
            }
        }

        #endregion
    }
}
