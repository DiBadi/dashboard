﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    /// <summary>
    /// A button that only triggers events when kept pressed for the given amount of time
    /// </summary>
    class LongPressButton : Button
    {
        #region Fields

        Button button;
        public EventHandler ButtonFired;
        Timer seconds;
        int countdown;
        int countdownState;
        string text;
        bool buttonDown;
        bool fired;
        bool oneTimeUse;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of a button that only fires when the pre-programmed time period 
        /// </summary>
        /// <param name="parent">The control that will be the parent object</param>
        /// <param name="buttonLocation">Position relative to the parent control</param>
        /// <param name="buttonSize">Size of the button</param>
        /// <param name="buttonText">Text of the button</param>
        /// <param name="timerInSeconds">Countdown timer of the button</param>
        public LongPressButton(Control parent, Point buttonLocation, Size buttonSize, string buttonText, int timerInSeconds, bool oneTimeUse)
        {
            buttonDown = false;
            fired = false;
            this.oneTimeUse = oneTimeUse;

            countdown = timerInSeconds + 1;
            countdownState = countdown;
            text = buttonText.ToUpper();

            seconds = new Timer();
            seconds.Interval = 1000;
            seconds.Tick += Seconds_Tick;

            button = new Button();
            button.Parent = parent;
            button.Location = buttonLocation;
            button.Size = buttonSize;
            button.FlatStyle = UIProperties.Button_FlatStyle_Main;
            button.MouseDown += Button_MouseDown;
            button.MouseUp += Button_MouseUp;
            button.MouseLeave += Button_MouseLeave;
            button.Show();

            UpdateButton();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Updates the button text
        /// </summary>
        private void UpdateButton()
        {
            button.Text = text;

            if (buttonDown)
            {
                button.Text += " (" + countdownState + ")";
            }

            if (fired)
            {
                this.button.Enabled = false;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Update timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seconds_Tick(object sender, EventArgs e)
        {
            if (countdownState > 0)
            {
                countdownState--;
            }

            if (countdownState == 0)
            {
                if (oneTimeUse)
                {
                    fired = true;
                }

                OnButtonFired();
            }

            UpdateButton();
        }

        /// <summary>
        /// Resets the countdown if the mouse leaves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseLeave(object sender, EventArgs e)
        {
            if (!fired)
            {
                buttonDown = false;
                seconds.Enabled = false;
                countdownState = countdown;
                UpdateButton();
            }
        }

        /// <summary>
        /// Resets the timer when the mouse is released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseUp(object sender, MouseEventArgs e)
        {
            if (!fired)
            {
                buttonDown = false;
                seconds.Enabled = false;
                countdownState = countdown;
                UpdateButton();
            }
        }

        /// <summary>
        /// Enables the countdown timer when the muse is down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_MouseDown(object sender, MouseEventArgs e)
        {
            if (!seconds.Enabled)
            {
                seconds.Enabled = true;
            }

            buttonDown = true;
        }

        /// <summary>
        /// Button fired event
        /// </summary>
        protected virtual void OnButtonFired()
        {
            if (ButtonFired != null)
            {
                ButtonFired(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
