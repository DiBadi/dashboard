﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INDEDashboard
{
    /// <summary>
    /// Contains methods and values for the PC lock and unlock
    /// </summary>
    class PCAdminSettings
    {
        #region Public methods

        public void Lock()
        {
            // Disable command prompt
            // HKEY_CURRENT_USER\SOFTWARE\Policies\Microsoft\Windows\System
            // DisableCMD value and set the value to 1
            // HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer
            // NoRun value to 1

            // Disable windows hotkey combos
            // HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer
            // DWORD NoWinKeys 1

            // Disable Administartive tools
            // HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
            //  StartMenuAdminTools to 0

            // Disable drive browsing

            // Disable administrative snap ins
            // HKCU\Software\Policies\Microsoft\MMC  RestrictToPermittedSnapins 1

            // disable registry editing
            // HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System   DisableRegistryTools  1


            HKLMAccess. SetStringValue(@"Software\Policies\Microsoft\Windows\System", "DisableCMD", "1");
        }

        public void Unlock()
        {
            HKCUAccess.SetStringValue(@"Software\Policies\Microsoft\Windows\System", "DisableCMD", "");
        }

        #endregion
    }
}