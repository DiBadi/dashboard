﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace INDEDashboard
{
    /// <summary>
    /// Accesses the HKLM key in the registry
    /// </summary>
    public static class HKLMAccess
    {

        /// <summary>
        /// Gets a string from the given key
        /// </summary>
        /// <param name="subKey">Name of the registry subkey</param>
        /// <param name="valueName">Name of the registry value</param>
        /// <returns>The string from the registry value</returns>
        public static string GetString(string subKey, string valueName)
        {
            using (RegistryKey regLocalMachine = Registry.LocalMachine.OpenSubKey(subKey))
            {
                Console.WriteLine("GETTING STRING " + valueName);
                string found = (string)regLocalMachine.GetValue(valueName);
                Console.WriteLine("DONE " + found);

                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a string from the given registry item. Returns empty if the query was unsuccesful
        /// </summary>
        /// <param name="subKey"></param>
        /// <param name="valueName"></param>
        /// <returns></returns>
        public static string[] GetStringArray(string subKey, string valueName)
        {
            string[] found;

            using (RegistryKey regLocalMachine = Registry.LocalMachine.OpenSubKey(subKey))
            {
                found = (string[])regLocalMachine.GetValue(valueName);

                if (found != null)
                {
                    foreach (var item in found)
                    {
                        Console.WriteLine("IN ARRAY " + item);
                    }
                }
            }

            return found;
        }

        public static void SetStringValue(string subKey, string valueName, string value)
        {
            using (RegistryKey regLocalMacine = Registry.LocalMachine.OpenSubKey(subKey))
            {
                try
                {
                    Registry.LocalMachine.SetValue(subKey + @"\" + valueName, valueName);

                    regLocalMacine.SetValue(valueName, value);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
