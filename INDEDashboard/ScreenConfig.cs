﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INDEDashboard
{
    /// <summary>
    /// Enumeration of output screens
    /// </summary>
    public enum OutputScreen
    {
        none,
        primary,
        secondary
    }

    /// <summary>
    /// Used to store and check screen settings 
    /// </summary>
    public class ScreenConfig
    {
        #region Fields

        #endregion

        #region Properties

        /// <summary>
        /// Screen config friendly name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Primary screen width
        /// </summary>
        public int PrimaryWidth { get; set; }
        /// <summary>
        /// Primary screen height
        /// </summary>
        public int PrimaryHeight { get; set; }
        /// <summary>
        /// Secondary screen width
        /// </summary>
        public int SecondaryWidth { get; set; }
        /// <summary>
        /// Secondary screen height
        /// </summary>
        public int SecondaryHeight { get; set; }
        /// <summary>
        /// Destination screen for the given application
        /// </summary>
        public OutputScreen Output { get; set; }

        #endregion

        #region Constructors

        public ScreenConfig()
        {
            Name = string.Empty;
            PrimaryWidth = -1;
            PrimaryHeight = -1;
            SecondaryWidth = -1;
            SecondaryHeight = -1;
            Output = OutputScreen.none;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Checks whether or not the requested screen configuration is available
        /// </summary>
        /// <returns>True means the screen configuration is available</returns>
        public bool Available()
        {
            bool screenCheck = true;

            System.Windows.Forms.Screen[] screensPresent = System.Windows.Forms.Screen.AllScreens;
            List<System.Windows.Forms.Screen> screens = new List<System.Windows.Forms.Screen>();

            if (screensPresent.Length > 0)
            {
                foreach (var item in screensPresent)
                {
                    if (item.Primary)
                    {
                        screens.Insert(0, item);
                    }
                    else
                    {
                        screens.Add(item);
                    }
                }
            }

            if (screens.Count > 0)
            {
                Console.WriteLine(Name + " OUTPUT " + Output);

                switch (Output)
                {                    
                    case OutputScreen.none:
                        screenCheck = true;
                        break;

                    case OutputScreen.primary:
                        if (screens[0].Bounds.Width == PrimaryWidth && screens[0].Bounds.Height == PrimaryHeight)
                        {
                            screenCheck = true;
                        }
                        else
                        {
                            screenCheck = false;
                        }
                        break;

                    case OutputScreen.secondary:
                        if (screens.Count > 1)
                        {
                            if (screens[0].Bounds.Width == PrimaryWidth && screens[0].Bounds.Height == PrimaryHeight)
                            {
                                for (int i = 1; i < screens.Count; i++)
                                {
                                    if (screens[i].Bounds.Width != SecondaryWidth || screens[i].Bounds.Height != SecondaryHeight)
                                    {
                                        screenCheck = false;
                                    }
                                }
                            }
                            else
                            {
                                screenCheck = false;
                            }
                        }
                        else
                        {
                            screenCheck = false;
                        }

                        break;
                }
            }
            else
            {
                screenCheck = false;
            }

            Console.WriteLine(Name + " Screen available " + screenCheck, false, false);
            return screenCheck;
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
