﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INDEDashboard
{
    public partial class Form1 : Form
    {
        #region Fields

        string dashboardProcessPath;
        string[] startArgs;
        bool adminStart = false;
        int selectedTab = -1;
        bool incidentalExit;
        bool forcedShutdown;
        bool readyToSleep;

        // Stores the settings for the current operation
        INI ini;

        // Dummy buttons for the start up
        AppToggleButton dummyMainAppButton;
        AppToggleButton dummyOnDemandButton;
        LongPressButton shutdownButton;

        // Network adapter UI
        NetworkAdapters networkAdapters;
        IPTextboxPanel ipAddressUI;
        IPTextboxPanel subnetMaskUI;
        IPTextboxPanel gatewayUI;
        IPTextboxPanel dns1UI;
        IPTextboxPanel dns2UI;
        LongPressButton applySettingsToInterface;

        // Update timer
        Timer updateTimer = new Timer();
        Timer shutdownTimer;

        #endregion

        #region Initialisation

        /// <summary>
        /// Initialises the form
        /// </summary>
        public Form1(string[] args)
        {
            InitializeComponent();

            incidentalExit = true;
            forcedShutdown = false;
            dashboardProcessPath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            readyToSleep = false;
            Microsoft.Win32.SystemEvents.PowerModeChanged += SystemEvents_PowerModeChanged;

            UDPDebug.Write.path = @"c:\tools\standalonedebugconsole.exe";
            UDPDebug.Write.Clear();

            // analise startup arguments
            if (args != null && args.Contains("adminstart"))
            {
                adminStart = true;
                UDPDebug.Write.Yellow("Started as admin", false);
            }

            // add UI elements to the schedule tab and initialise its components
            ini = new INI();

            // create network adapter UI 
            networkAdapters = new NetworkAdapters();
            ipAddressUI = new IPTextboxPanel(tabpNetwork, 123, 31);
            subnetMaskUI = new IPTextboxPanel(tabpNetwork, 123, 57);
            gatewayUI = new IPTextboxPanel(tabpNetwork, 123, 83);
            dns1UI = new IPTextboxPanel(tabpNetwork, 123, 109);
            dns2UI = new IPTextboxPanel(tabpNetwork, 123, 135);
            applySettingsToInterface = new LongPressButton(tabpNetwork, new Point(123, 163), new Size(135, 25),
                "APPLY", 15, false);

            DisableNetworkAdapterUI();

            // add dummy buttons to the launch tab
            dummyMainAppButton = new AppToggleButton(tabpLaunch, "", UIProperties.Button_MainApp_Location, UIProperties.Button_Size_Main,
                true, State.stopped);
            dummyMainAppButton.UpdateButton(0, State.check, "LOADING...");
            dummyOnDemandButton = new AppToggleButton(tabpLaunch, "", UIProperties.Button_OnDemandApp_Location, UIProperties.Button_Size_Main,
                true, State.stopped);
            dummyOnDemandButton.UpdateButton(0, State.check, "SELECT AN APP");
            dummyOnDemandButton.Button.Enabled = false;
            dummyOnDemandButton.Button.Text = "SELECT";

            // add shutdown button to the lauch tab
            shutdownButton = new LongPressButton(tabpLaunch, new Point(212, 190), new Size(174, 31), "shutdown", 5, true);
            shutdownButton.ButtonFired += ShutdownButton;

            // Initialise update timer
            updateTimer.Interval = 2000;
            updateTimer.Tick += UpdateTimer_Tick;
            updateTimer.Enabled = true;
        }

        /// <summary>
        /// Gets called when the main form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // Load ini file
            ini.LoadINI(adminStart);

            List<string> appsToCombo = new List<string>();

            if (adminStart)
            {
                foreach (var item in ini.Applications)
                {
                    if (item.AppType == AppType.main || item.AppType == AppType.ondemand || item.AppType == AppType.website)
                    {
                        // initialise apps
                        switch (item.AppType)
                        {
                            case AppType.main:
                                dummyMainAppButton.ButtonVisible = false;
                                item.ScheduleUIParent = tabpSchedule;
                                item.ButtonParent = tabpLaunch;
                                item.ShutdownSequenceStart += OnScheduledShutdownStart;                        
                                break;
                            case AppType.ondemand:
                                appsToCombo.Add(item.FriendlyName);
                                item.ButtonParent = tabpLaunch;
                                break;
                            case AppType.website:
                                appsToCombo.Add(item.FriendlyName);
                                item.ButtonParent = tabpLaunch;
                                break;
                        }
                    }

                    item.Initialize(true);
                }

                // populate select ondemand application selector combo
                if (appsToCombo.Count > 0)
                {
                    appsToCombo.Sort();

                    foreach (var item in appsToCombo)
                    {
                        comboOnDemandAppSelect.Items.Add(item);
                    }
                }
            }
            else
            {
                Label initLabel = new Label();
                initLabel.Text = "loading...";
                initLabel.Location = new Point(10, 10);
                initLabel.Parent = this;
                initLabel.Show();

                AvoidMultipleinstances();

                tabControl1.Visible = false;

                RestartAsAdmin();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Closes the application if multiple instances are running
        /// </summary>
        private void AvoidMultipleinstances()
        {
            string processPath = (System.IO.Path.GetFullPath(dashboardProcessPath));
            string processName = (System.IO.Path.GetFileName(processPath)).Remove(System.IO.Path.GetFileName(processPath).IndexOf('.'));

            System.Diagnostics.Process[] runningProcesses = System.Diagnostics.Process.GetProcesses();
            int instances = 0;

            foreach (var item in runningProcesses)
            {
                if (item.ProcessName == processName)
                {
                    instances++;
                }
            }

            if (instances > 1)
            {
                System.Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Restarts the current app as an admin
        /// </summary>
        private void RestartAsAdmin()
        {
            System.Diagnostics.Process newDashboard = new System.Diagnostics.Process();
            newDashboard.StartInfo.FileName = dashboardProcessPath;
            newDashboard.StartInfo.Arguments = "adminstart";

            // needed for testing (removes vshost from the filename in debugging mode)
            if (newDashboard.StartInfo.FileName.Contains(".vshost."))
            {
                newDashboard.StartInfo.FileName = newDashboard.StartInfo.FileName.Remove(newDashboard.StartInfo.FileName.IndexOf(".vshost."), 7);
            }

            if (!string.IsNullOrEmpty(INI.dashboardUser))
            {
                foreach (var item in INI.credentials)
                {
                    if (item.Username == INI.dashboardUser)
                    {
                        newDashboard.StartInfo.UserName = item.Username;
                        newDashboard.StartInfo.Password = item.Password;
                        newDashboard.StartInfo.UseShellExecute = false;
                        newDashboard.StartInfo.LoadUserProfile = true;
                    }
                }
            }

            try
            {
                newDashboard.Start();
            }
            catch (Exception)
            {
            }

            try
            {
                Environment.Exit(0);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Starts the sleep or shutdown process
        /// </summary>
        private void InitiateSleepOrShutdown()
        {
            foreach (var item in ini.Applications)
            {
                item.ScheduledShutdown = true;
            }

            shutdownTimer = new Timer();
            shutdownTimer.Interval = UIProperties.App_Termination_Delay_Millisec;
            shutdownTimer.Tick += ShutdownTimer_Tick;
            shutdownTimer.Enabled = true;
        }

        #endregion

        #region UI Methods

        /// <summary>
        /// This method gets called when different tabs being selected on the main tab control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // save schedule data when schedule tab is unselected
            if (tabControl1.SelectedIndex != 1 && selectedTab == 1)
            {
                // TODO save schedule here
                foreach (var item in ini.Applications)
                {
                    item.WriteSchedule();
                }
            }

            selectedTab = tabControl1.SelectedIndex;
        }

        /// <summary>
        /// Updates the on demand app button when a new item is selected from the dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboSelectApplication_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(comboOnDemandAppSelect.SelectedItem.ToString()))
            {
                dummyOnDemandButton.ButtonVisible = false;

                foreach (var item in ini.Applications)
                {
                    if (comboOnDemandAppSelect.SelectedItem.ToString() == item.FriendlyName)
                    {
                        item.UIVisible = true;
                    }
                    else
                    {
                        if (item.AppType != AppType.main)
                        {
                            item.UIVisible = false;
                        }
                    }
                }
            }
            else
            {
                dummyOnDemandButton.ButtonVisible = true;

                foreach (var item in ini.Applications)
                {
                    if (item.AppType != AppType.main)
                    {
                        item.appToggleButton.ButtonVisible = false;
                    }
                }
            }
        }

        /// <summary>
        /// Searches for available network adapters when the network adapters combo box is opened
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboInterfaces_DropDown(object sender, EventArgs e)
        {
            comboInterfaces.Items.Clear();

            foreach (var item in networkAdapters.GetNetworkAdapters())
            {
                if (item.NetworkInterfaceType == System.Net.NetworkInformation.NetworkInterfaceType.Wireless80211 ||
                    item.NetworkInterfaceType == System.Net.NetworkInformation.NetworkInterfaceType.Ethernet)
                {
                    comboInterfaces.Items.Add(item.Name);
                }
            }
        }

        /// <summary>
        /// Populates the network adapter controls if possible
        /// </summary>
        /// <param name="adapterName"></param>
        private void UpdateNetworAdapterUI(string adapterName)
        {
            string ipAddress;
            string subnetMask;
            string gateway;
            bool autoDns;
            string dns1 = string.Empty;
            string dns2 = string.Empty;

            bool isConnected;

            if (networkAdapters.IsDhcp(adapterName, out isConnected, out ipAddress, out subnetMask, out gateway, out autoDns, out dns1, out dns2))
            {
                checkAdapterIPAuto.Checked = true;

                ipAddressUI.Enabled(false);
                subnetMaskUI.Enabled(false);
                gatewayUI.Enabled(false);
                dns1UI.Enabled(false);
                dns2UI.Enabled(false);
            }
            else
            {
                checkAdapterIPAuto.Checked = false;

                ipAddressUI.Enabled(true);
                subnetMaskUI.Enabled(true);
                gatewayUI.Enabled(true);
                dns1UI.Enabled(true);
                dns2UI.Enabled(true);
            }

            ipAddressUI.IPString = ipAddress;
            gatewayUI.IPString = gateway;
            subnetMaskUI.IPString = subnetMask;

            dns1UI.IPString = dns1;
            dns2UI.IPString = dns2;

            checkAdapterDNSAuto.Checked = autoDns;
        }

        /// <summary>
        /// Disables controls on the network adapter tab
        /// </summary>
        private void DisableNetworkAdapterUI()
        {
            ipAddressUI.Enabled(false);
            subnetMaskUI.Enabled(false);
            gatewayUI.Enabled(false);
            dns1UI.Enabled(false);
            dns2UI.Enabled(false);
        }

        /// <summary>
        /// Gets called when the dashboard shutdown button is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShutdownButton(object sender, EventArgs e)
        {
            shutdownButton.Enabled = false;
            forcedShutdown = true;
            InitiateSleepOrShutdown();
        }

        /// <summary>
        /// Updates the network adapter configuration related textboxes and checkboxes, when a new item is selected 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboInterfaces_SelectedValueChanged(object sender, EventArgs e)
        {
            ipAddressUI.CLear();
            subnetMaskUI.CLear();
            gatewayUI.CLear();
            dns1UI.CLear();
            dns2UI.CLear();

            if (!string.IsNullOrEmpty(comboInterfaces.SelectedItem.ToString()))
            {
                UpdateNetworAdapterUI(comboInterfaces.SelectedItem.ToString());
            }
            else
            {
                DisableNetworkAdapterUI();
            }
        }

        /// <summary>
        /// Gets called when the DHCP checkbox is clicked
        /// Toggles the network adapter configuration related textboxes and the auto DNS checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkAdapterIPAuto_CheckedChanged(object sender, EventArgs e)
        {
            ipAddressUI.Enabled(!checkAdapterIPAuto.Checked);
            subnetMaskUI.Enabled(!checkAdapterIPAuto.Checked);
            gatewayUI.Enabled(!checkAdapterIPAuto.Checked);

            checkAdapterDNSAuto.Checked = checkAdapterIPAuto.Checked;
        }

        private void checkAdapterDNSAuto_CheckedChanged(object sender, EventArgs e)
        {
            dns1UI.Enabled(!checkAdapterDNSAuto.Checked);
            dns2UI.Enabled(!checkAdapterDNSAuto.Checked);

            if (!checkAdapterIPAuto.Checked)
            {
                checkAdapterDNSAuto.Checked = false;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Gets called when a scheduled shutdown event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnScheduledShutdownStart(object sender, EventArgs args)
        {
            InitiateSleepOrShutdown();
        }

        /// <summary>
        /// Executes the scheduled shutdown/sleep after the termination delay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShutdownTimer_Tick(object sender, EventArgs e)
        {
            shutdownTimer.Enabled = false;

            if (!readyToSleep)
            {
                foreach (var item in ini.Applications)
                {
                    item.Kill(true);
                }

                readyToSleep = true;

                shutdownTimer.Interval = 10000;
                shutdownTimer.Enabled = true;
            }
            else
            {
                // shutting down for button press
                if (forcedShutdown)
                {
                    System.Diagnostics.Process.Start("shutdown.exe", "/s /t 20");
                    incidentalExit = false;
                    Application.Exit();
                }
                else
                {
                    if (INI.switchOff)
                    {
                        System.Diagnostics.Process.Start("shutdown.exe", "/s /t 30");
                        incidentalExit = false;
                        Application.Exit();
                    }
                    else
                    {
                        Application.SetSuspendState(PowerState.Suspend, false, false);
                    }
                }
            }
        }

        /// <summary>
        /// Update timer elapsed - checks and launches necessary apps and updates UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            AvoidMultipleinstances();
            updateTimer.Enabled = false;
        }

        /// <summary>
        /// Restarts the dashboard, when the PC resumes from the sleep state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SystemEvents_PowerModeChanged(object sender, Microsoft.Win32.PowerModeChangedEventArgs e)
        {
            if (e.Mode == Microsoft.Win32.PowerModes.Resume)
            {
                RestartAsAdmin();
            }
        }

        /// <summary>
        /// Re-launches the dashboard if it's closed accidentally
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (adminStart && incidentalExit)
            {
                // TODO: UNCOMMENT FOR BUILDS
                RestartAsAdmin();
            }
        }

        #endregion
    }
}
