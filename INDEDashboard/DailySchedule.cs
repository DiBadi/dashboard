﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    /// <summary>
    /// Stores daily schedule elements and defines graphic content
    /// </summary>
    public class DailySchedule
    {
        #region Fields

        private int active;
        private int startHour;
        private int startMinute;
        private int startPC;
        private int stopHour;
        private int stopMinute;
        private int shutdown;

        #endregion

        #region Properties

        /// <summary>
        /// Stores the day identifier
        /// </summary>
        public int Day { get; set; }
        /// <summary>
        /// Stores the active status. 1 for active 0 for inactive
        /// </summary>
        public int Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;

                if (value == 1)
                {
                    CheckDayActive.Checked = true;
                }
                else
                {
                    CheckDayActive.Checked = false;
                }
            }
        }
        /// <summary>
        /// Stores the start hour
        /// </summary>
        public int StartHour
        {
            get
            {
                return startHour;
            }
            set
            {
                startHour = value;
                TbStartHour.Text = value.ToString();
            }
        }
        /// <summary>
        /// Stores the start minute
        /// </summary>
        public int StartMinute
        {
            get
            {
                return startMinute;
            }
            set
            {
                startMinute = value;
                TbStartMinute.Text = value.ToString();
            }
        }
        /// <summary>
        /// Shows whether or not the PC needs to be started on schedule
        /// </summary>
        public int StartPC
        {
            get
            {
                return startPC;
            }
            set
            {
                if (value == 1)
                {
                    CheckStartPC.Checked = true;
                    startPC = 1;
                }
                else
                {
                    CheckStartPC.Checked = false;
                    startPC = 0;
                }
            }
        }
        /// <summary>
        /// Stores the stop hour
        /// </summary>
        public int StopHour
        {
            get
            {
                return stopHour;
            }
            set
            {
                stopHour = value;
                TbStopHour.Text = value.ToString();
            }
        }
        /// <summary>
        /// Stores The stop minute
        /// </summary>
        public int StopMinute
        {
            get
            {
                return stopMinute;
            }
            set
            {
                stopMinute = value;
                TbStopMinute.Text = value.ToString();
            }
        }
        /// <summary>
        /// Shows whether the PC needs to be shutdown after the schedule has been executed
        /// </summary>
        public int Shutdown
        {
            get
            {
                return shutdown;
            }
            set
            {
                shutdown = value;

                if (value == 1)
                {
                    CheckStopPC.Checked = true;
                }
                else
                {
                    CheckStopPC.Checked = false;
                }
            }
        }
        /// <summary>
        /// UI elements properties
        /// </summary>
        public CheckBox CheckDayActive { get; set; }
        public TextBox TbStartHour { get; set; }
        public TextBox TbStartMinute { get; set; }
        public CheckBox CheckStartPC { get; set; }
        public TextBox TbStopHour { get; set; }
        public TextBox TbStopMinute { get; set; }
        public CheckBox CheckStopPC { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Basic constructor for storing data
        /// </summary>
        public DailySchedule()
        {
            active = 0;
            startHour = -1;
            startMinute = -1;
            startPC = 0;
            stopHour = -1;
            stopMinute = -1;
            shutdown = 0;

            Panel newLine = GetScheduleLine(0);
            newLine.Parent = null;
            newLine.Hide();
        }

        /// <summary>
        /// Constructor to create winfor controls
        /// </summary>
        /// <param name="day">0 - Monday, 1 - Tuesday ... 6 - Sunday</param>
        /// <param name="parent">Target page</param>
        public DailySchedule(int day, Control parent)
        { 
            Day = day;
            active = 0;
            startHour = -1;
            startMinute = -1;
            startPC = 0;
            stopHour = -1;
            stopMinute = -1;
            shutdown = 0;

            // Initializes a new line and fills it with necessary controls
            Panel newLine = GetScheduleLine(day);
            newLine.Parent = parent;
            newLine.Show();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Fills the Panel with the necessary controls
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        private Panel GetScheduleLine(int day)
        {
            Panel newLine = new Panel();
            newLine.Size = UIProperties.Schedule_Line_Panel_Size;
            newLine.Location = new Point(UIProperties.Schedule_Line_Panel_Location.X, (UIProperties.Schedule_Line_Panel_Location.Y + (day * newLine.Size.Height)));

            CheckDayActive = new CheckBox();
            CheckDayActive.ForeColor = Color.Black;
            CheckDayActive.Location = new Point(0, 0);
            CheckDayActive.Size = new Size(90, 24);
            switch (day)
            {
                case 0:
                    CheckDayActive.Text = "Monday";
                    break;
                case 1:
                    CheckDayActive.Text = "Tuesday";
                    break;
                case 2:
                    CheckDayActive.Text = "Wednesday";
                    break;
                case 3:
                    CheckDayActive.Text = "Thursday";
                    break;
                case 4:
                    CheckDayActive.Text = "Friday";
                    break;
                case 5:
                    CheckDayActive.Text = "Saturday";
                    break;
                case 6:
                    CheckDayActive.Text = "Sunday";
                    break;
            }
            CheckDayActive.CheckedChanged += CheckDayActive_CheckedChanged;
            CheckDayActive.Click += CheckDayActive_Click;
            CheckDayActive.Parent = newLine;
            CheckDayActive.Show();

            TbStartHour = new TextBox();
            TbStartHour.Location = new Point(100, 0);
            TbStartHour.MaxLength = 2;
            TbStartHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            TbStartHour.ForeColor = UIProperties.FontColor_Default;
            TbStartHour.Size = new Size(24, 20);
            TbStartHour.Text = "";
            TbStartHour.TextChanged += StartHour_TextChanged;
            TbStartHour.Parent = newLine;
            TbStartHour.Show();

            TbStartMinute = new TextBox();
            TbStartMinute.Location = new Point(130, 0);
            TbStartMinute.MaxLength = 2;
            TbStartMinute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            TbStartMinute.ForeColor = UIProperties.FontColor_Default;
            TbStartMinute.Size = new Size(24, 20);
            TbStartMinute.Text = "";
            TbStartMinute.TextChanged += TbStartMinute_TextChanged;
            TbStartMinute.Parent = newLine;
            TbStartMinute.Show();

            CheckStartPC = new CheckBox();
            CheckStartPC.Location = new Point(165, 0);
            CheckStartPC.CheckedChanged += CheckStartPC_CheckedChanged;
            CheckStartPC.Size = new Size(24, 24);
            CheckStartPC.Parent = newLine;
            CheckStartPC.Show();

            TbStopHour = new TextBox();
            TbStopHour.Location = new Point(215, 0);
            TbStopHour.MaxLength = 2;
            TbStopHour.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            TbStopHour.ForeColor = UIProperties.FontColor_Default;
            TbStopHour.Size = new Size(24, 20);
            TbStopHour.Text = "";
            TbStopHour.TextChanged += TbStopHour_TextChanged;
            TbStopHour.Parent = newLine;
            TbStopHour.Show();

            TbStopMinute = new TextBox();
            TbStopMinute.Location = new Point(245, 0);
            TbStopMinute.MaxLength = 2;
            TbStopMinute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            TbStopMinute.ForeColor = UIProperties.FontColor_Default;
            TbStopMinute.Size = new Size(24, 20);
            TbStopMinute.Text = "";
            TbStopMinute.TextChanged += TbStopMinute_TextChanged;
            TbStopMinute.Parent = newLine;
            TbStopMinute.Show();

            CheckStopPC = new CheckBox();
            CheckStopPC.Location = new Point(278, 0);
            CheckStopPC.CheckedChanged += CheckStopPC_CheckedChanged;
            CheckStopPC.Parent = newLine;
            CheckStopPC.Show();

            return newLine;
        }

        /// <summary>
        /// Validates the string. Returns -1 if the value was not a valid hour or minute.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="hour"></param>
        /// <returns></returns>
        private int CheckHourOrMinuteTextbox(string text, bool hour)
        {
            int prs = -1;

            if (!int.TryParse(text, out prs))
            {
                return -1;
            }
            else
            {
                if (prs > -1 && prs < 24 && hour)
                {
                    return prs;
                }
                else if (prs > -1 && prs < 60 && !hour)
                {
                    return prs;
                }
                else
                {
                    return -1;
                }
            }
        }

        #endregion

        #region Events

        private void CheckDayActive_Click(object sender, EventArgs e)
        {
            if (CheckDayActive.Checked)
            {
                active = 1;
            }
            else
            {
                active = 0;
                CheckStartPC.Checked = false;
            }
        }

        private void CheckDayActive_CheckedChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Validates the start hour textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartHour_TextChanged(object sender, EventArgs e)
        {
            CheckDayActive.Checked = false;
            active = 0;

            int val = CheckHourOrMinuteTextbox(TbStartHour.Text, true);

            if (val > -1)
            {
                startHour = val;
            }
            else
            {
                startHour = -1;
                TbStartHour.Text = "";
            }
        }

        /// <summary>
        /// Validates the start minute textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbStartMinute_TextChanged(object sender, EventArgs e)
        {
            CheckDayActive.Checked = false;
            active = 0;

            int val = CheckHourOrMinuteTextbox(TbStartMinute.Text, false);

            if (val > -1)
            {
                startMinute = val;
            }
            else
            {
                startMinute = -1;
                TbStartMinute.Text = "";
            }
        }

        private void CheckStartPC_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckStartPC.Checked)
            {
                if (startHour != -1 && startMinute != -1)
                {
                    StartPC = 1;
                }
                else
                {
                    CheckStartPC.Checked = false;
                    StartPC = 0;
                }
            }
            else
            {
                startPC = 0;
            }
        }

        /// <summary>
        /// Validates the stop hour textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbStopHour_TextChanged(object sender, EventArgs e)
        {
            CheckDayActive.Checked = false;
            active = 0;

            int val = CheckHourOrMinuteTextbox(TbStopHour.Text, true);

            if (val > -1)
            {
                stopHour = val;
            }
            else
            {
                stopHour = -1;
                TbStopHour.Text = "";
            }
        }

        /// <summary>
        /// Validates the stop minute textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TbStopMinute_TextChanged(object sender, EventArgs e)
        {
            CheckDayActive.Checked = false;
            active = 0;

            int val = CheckHourOrMinuteTextbox(TbStopMinute.Text, false);

            if (val > -1)
            {
                stopMinute = val;
            }
            else
            {
                stopMinute = -1;
                TbStopMinute.Text = "";
            }
        }

        private void CheckStopPC_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckStopPC.Checked)
            {
                shutdown = 1;
            }
            else
            {
                shutdown = 0;
            }
        }

        #endregion
    }
}
