﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Net;

namespace INDEDashboard
{
    /// <summary>
    /// Contains four IPTextboxItems, validates the entered values
    /// </summary>
    class IPTextboxPanel
    {
        private string ipString;
        
        #region Properties

        /// <summary>
        /// Parent control of the IP textboxes
        /// </summary>
        public Panel Panel
        {
            get; set;
        }

        /// <summary>
        /// Validates and stores the IP address in string format.
        /// Updates the IPAddress property.
        /// Splits the IP and assigns the elements to the matching textboxes.
        /// </summary>
        public string IPString
        {
            get
            {
                return ipString;
            }
            set
            {
                System.Net.IPAddress parseIP;

                if (System.Net.IPAddress.TryParse(value, out parseIP))
                {
                    ipString = value;
                    IPAddress = parseIP;

                    string[] split = ipString.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                    if (split.Length == 4)
                    {
                        IPbox1.textbox.Text = split[0];
                        IPbox2.textbox.Text = split[1];
                        IPbox3.textbox.Text = split[2];
                        IPbox4.textbox.Text = split[3];
                    }
                }
            }
        }

        public IPAddress IPAddress
        {
            get; set;
        }

        public IPTextboxItem IPbox1
        {
            get; set;
        }
        public IPTextboxItem IPbox2
        {
            get; set;
        }
        public IPTextboxItem IPbox3
        {
            get; set;
        }
        public IPTextboxItem IPbox4
        {
            get; set;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initialises an auto-validating IP textbox controll
        /// </summary>
        /// <param name="parentPage">Controll gets linked to this object</param>
        /// <param name="locationX">Horizontal position</param>
        /// <param name="locationY">Vertical position</param>
        public IPTextboxPanel(TabPage parentPage, int locationX, int locationY)
        {
            Panel = new Panel();
            Panel.Location = new Point(locationX, locationY);
            Panel.Width = UIProperties.IPTextbox_Item_Size.Width * 4 + 21;
            Panel.Height = UIProperties.IPTextbox_Item_Size.Height + 1;
            Panel.BackColor = UIProperties.BackColour_Default;

            Panel.Parent = parentPage;
            Panel.Show();

            IPbox1 = new IPTextboxItem(Panel, 0, 0, 0);
            IPbox2 = new IPTextboxItem(Panel, 1, UIProperties.IPTextbox_Item_Size.Width + 5, 0);
            IPbox3 = new IPTextboxItem(Panel, 2, (UIProperties.IPTextbox_Item_Size.Width + 5) * 2, 0);
            IPbox4 = new IPTextboxItem(Panel, 3, (UIProperties.IPTextbox_Item_Size.Width + 5) * 3, 0);

            IPbox1.LeftArrowPressed += IPbox1_LeftArrowPressed;
            IPbox1.RightArrowPressed += IPbox1_RightArrowPressed;
            IPbox2.LeftArrowPressed += IPbox2_LeftArrowPressed;
            IPbox2.RightArrowPressed += IPbox2_RightArrowPressed;
            IPbox3.LeftArrowPressed += IPbox3_LeftArrowPressed;
            IPbox3.RightArrowPressed += IPbox3_RightArrowPressed;
            IPbox4.LeftArrowPressed += IPbox4_LeftArrowPressed;
            IPbox4.RightArrowPressed += IPbox4_RightArrowPressed;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Clears the IP textbox
        /// </summary>
        public void CLear()
        {
            IPbox1.textbox.Text = string.Empty;
            IPbox2.textbox.Text = string.Empty;
            IPbox3.textbox.Text = string.Empty;
            IPbox4.textbox.Text = string.Empty;
        }

        public void Enabled(bool enabled)
        {
            IPbox1.textbox.Enabled = enabled;
            IPbox2.textbox.Enabled = enabled;
            IPbox3.textbox.Enabled = enabled;
            IPbox4.textbox.Enabled = enabled;
        }

        /// <summary>
        /// Validates and returns (if possible) the IP address from the IP textbox field
        /// </summary>
        /// <param name="ipAddress">Parsed IP address</param>
        /// <returns></returns>
        public bool GetIP(out IPAddress ipAddress)
        {
            ipAddress = new IPAddress(0);

            if (System.Net.IPAddress.TryParse(IPbox1.textbox.Text + "." + IPbox2.textbox.Text + "."
                + IPbox3.textbox.Text + "." + IPbox4.textbox.Text, out ipAddress))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Private methods

        #endregion

        #region Events

        private void IPbox4_RightArrowPressed(object sender, EventArgs e)
        {
            IPbox1.textbox.Focus();
        }

        private void IPbox3_RightArrowPressed(object sender, EventArgs e)
        {
            IPbox4.textbox.Focus();
        }

        private void IPbox2_RightArrowPressed(object sender, EventArgs e)
        {
            IPbox3.textbox.Focus();
        }

        private void IPbox1_RightArrowPressed(object sender, EventArgs e)
        {
            IPbox2.textbox.Focus();
        }

        private void IPbox4_LeftArrowPressed(object sender, EventArgs e)
        {
            IPbox3.textbox.Focus();
        }

        private void IPbox3_LeftArrowPressed(object sender, EventArgs e)
        {
            IPbox2.textbox.Focus();
        }

        private void IPbox2_LeftArrowPressed(object sender, EventArgs e)
        {
            IPbox1.textbox.Focus();
        }

        private void IPbox1_LeftArrowPressed(object sender, EventArgs e)
        {
            IPbox4.textbox.Focus();
        }

        #endregion
    }
}
