﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace INDEDashboard
{
    /// <summary>
    /// Cyphers and decyphers data
    /// </summary>
    class Crypto
    {
        #region Fields

        string pw;
        byte[] s;

        #endregion

        #region Constructors

        public Crypto(string pw, byte[] s)
        {
            this.pw = pw;
            this.s = s;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates an encrypted byte array
        /// </summary>
        /// <param name="bytesToBeEncrypted"></param>
        /// <returns></returns>
        public byte[] AES_Encrypt(byte[] bytesToBeEncrypted)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 7, 2, 5, 8, 2, 9, 8, 3 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (AesManaged AES = new AesManaged())
                {
                    AES.KeySize = 128;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(pw, s, 1024);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Padding = PaddingMode.PKCS7;
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        /// <summary>
        /// Decyphers an encrypted byte array
        /// </summary>
        /// <param name="bytesToBeDecrypted"></param>
        /// <returns></returns>
        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 7, 2, 5, 8, 2, 9, 8, 3 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (AesManaged AES = new AesManaged())
                {
                    AES.KeySize = 128;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(pw, s, 1024);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Padding = PaddingMode.PKCS7;
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                    }

                    decryptedBytes = ms.ToArray();

                    ms.Flush();
                    ms.Dispose();
                }
            }

            return decryptedBytes;
        }

        #endregion
    }
}
