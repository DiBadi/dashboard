﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INDEDashboard
{
    public enum PresetMessage
    {
        NoFile,
        BadFile,
        ScreenError,
        LaunchError,
        KillError,
        OffSchedule
    }

    /// <summary>
    /// Stores one app message with the associated expiry and error level. Manages expiration.
    /// </summary>
    public class AppInfo
    {
        #region Fields

        private int expiry;
        private string errorMessage;
        private int errorLevel;

        #endregion

        #region Properties

        /// <summary>
        /// Stores the error level. Read only.
        /// 0 - no error, just a message, 1 - minor error, 2- major error
        /// </summary>
        public int ErrorLevel
        {
            get
            {
                return errorLevel;
            }
        }

        /// <summary>
        /// Stores the message from the app. Read only.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
        }

        /// <summary>
        /// Strores the message expiry. Value updated at every tick of the update timer.
        /// </summary>
        public int Expiry
        {
            get
            {
                return expiry;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new app (error) message instance)
        /// </summary>
        /// <param name="message">Message string</param>
        /// <param name="errorLevel">Can be 0, 1, or 2. 0 is the default value.</param>
        /// <param name="expiresInSeconds"></param>
        public AppInfo(string message, int errorLevel, int expiresInTicks)
        {
            errorMessage = message;

            if (errorLevel >= 0 && errorLevel <= 2)
            {
                this.errorLevel = errorLevel;
            }
            else
            {
                this.errorLevel = 0;
            }

            expiry = expiresInTicks;
        }

        /// <summary>
        /// Instantiates a new app message, with preset parameters
        /// </summary>
        /// <param name="preset">Preset error message</param>
        public AppInfo(PresetMessage preset)
        {
            switch (preset)
            {
                case PresetMessage.NoFile:
                    errorMessage = "Unable to find file. Contact INDE support.";
                    errorLevel = 2;
                    expiry = 6000;
                    break;
                case PresetMessage.BadFile:
                    errorMessage = "Invalid file. Contact INDE support.";
                    errorLevel = 2;
                    expiry = 6000;
                    break;
                case PresetMessage.ScreenError:
                    errorMessage = "Incorrect screen configuration. Check settings or contact INDE support.";
                    errorLevel = 1;
                    expiry = 120;
                    break;
                case PresetMessage.LaunchError:
                    errorMessage = "Unable to launch. Contact INDE support.";
                    errorLevel = 2;
                    expiry = 6000;
                    break;
                case PresetMessage.KillError:
                    errorMessage = "Unable to properly terminate. If the error persists contact INDE support.";
                    errorLevel = 2;
                    expiry = 20;
                    break;
                case PresetMessage.OffSchedule:
                    errorMessage = "Off schedule operation";
                    errorLevel = 1;
                    expiry = 2;
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the expiry counter
        /// </summary>
        public bool CheckExpired()
        {
            if (expiry > 0)
            {
                expiry--;
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion
    }
}
