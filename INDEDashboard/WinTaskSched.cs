﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.TaskScheduler;

namespace INDEDashboard
{
    /// <summary>
    /// Writes and reads the windoes task scheduler to store dashboard schedule data
    /// </summary>
    public static class WinTaskSched
    {
        public static string startMethod;

        /// <summary>
        /// CReate weekly schedule for the day highlighted in the 'day' parameter
        /// </summary>
        /// <param name="day">0 - monday, 1 - tuesday ... 6 - sunday</param>
        /// <param name="active">1 means task is active</param>
        /// <param name="startHour">0-23</param>
        /// <param name="startMinute">0-59</param>
        /// <param name="switchOn">1 means the PC will be waken to execute this schedule</param>
        /// <param name="stopHour">0-23</param>
        /// <param name="stopMinute">0-59</param>
        /// <param name="switchOff">1 means the PC will be switched off after the player is shut down</param>
        public static void CreateWeeklySchedule(string applicationName, int day, int active, int startHour, int startMinute, int switchOn, 
            int stopHour, int stopMinute, int switchOff, string startMethod)
        {
            // creating weekly tasks for the selected days
            string scheduleName = "idpcs" + applicationName + "_";
            WeeklyTrigger trigger = new WeeklyTrigger();

            switch (day)
            {
                case 0:
                    scheduleName += "Mon";
                    trigger.DaysOfWeek = DaysOfTheWeek.Monday;
                    break;
                case 1:
                    scheduleName += "Tue";
                    trigger.DaysOfWeek = DaysOfTheWeek.Tuesday;
                    break;
                case 2:
                    scheduleName += "Wed";
                    trigger.DaysOfWeek = DaysOfTheWeek.Wednesday;
                    break;
                case 3:
                    scheduleName += "Thu";
                    trigger.DaysOfWeek = DaysOfTheWeek.Thursday;
                    break;
                case 4:
                    scheduleName += "Fri";
                    trigger.DaysOfWeek = DaysOfTheWeek.Friday;
                    break;
                case 5:
                    scheduleName += "Sat";
                    trigger.DaysOfWeek = DaysOfTheWeek.Saturday;
                    break;
                case 6:
                    scheduleName += "Sun";
                    trigger.DaysOfWeek = DaysOfTheWeek.Sunday;
                    break;
            }


            TaskDefinition definition = TaskService.Instance.NewTask();

            definition.RegistrationInfo.Description = "day=" + day + "|startHour=" + startHour +
            "|startMinute=" + startMinute + "|switchOn=" + switchOn + "|stopHour=" + stopHour + "|stopMinute=" + stopMinute +
            "|shutdown=" + switchOff + "|active=" + active + "|startmethod=" + startMethod;

            TimeSpan ts = new TimeSpan(startHour, startMinute, 0);
            DateTime start;

            if (startHour != -1 && startMinute != -1)
            {
                start = DateTime.Now.Date + ts;
            }
            else
            {
                start = new DateTime(2200, 1, 1, 1, 1, 1);
            }

            trigger.StartBoundary = start;
            trigger.Id = scheduleName;

            if (switchOn == 1)
            {
                trigger.Enabled = true;
            }
            else
            {
                trigger.Enabled = false;
            }

            definition.Settings.Hidden = false;
            definition.Triggers.Add(trigger);

            // define executable here
            definition.Actions.Add(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            if (switchOn == 1)
            {
                definition.Settings.WakeToRun = true;
                definition.Settings.Enabled = true;
            }
            else
            {
                definition.Settings.WakeToRun = false;
                definition.Settings.Enabled = false;
            }

            // write task in the INDE folder
            try
            {
                TaskService.Instance.RootFolder.RegisterTaskDefinition(@"INDE\" + scheduleName, definition);
            }
            catch (Exception)
            {
            }

            // disposing resources
            trigger.Dispose();
            definition.Dispose();
        }

        /// <summary>
        /// Returns a DailySchedule list based on the available scheduled task
        /// </summary>
        /// <returns></returns>
        public static List<DailySchedule> GetWeeklySchedules(string applicationName)
        {
            // set up reurn container
            List<DailySchedule> schedulesFound = new List<DailySchedule>();

            // List available tasks
            TaskCollection tasks = TaskService.Instance.RootFolder.GetTasks(new System.Text.RegularExpressions.Regex("/INDE/"));
            TaskFolderCollection taskFolders = TaskService.Instance.RootFolder.SubFolders;

            foreach (var folder in taskFolders)
            {
                if (folder.Name == "INDE")
                {
                    tasks = folder.GetTasks(null);
                }
            }

            // process task info
            foreach (var item in tasks)
            {
                if (item.Name.StartsWith("idpcs" + applicationName))
                {
                    DailySchedule schedule = new DailySchedule(); 

                    string[] split = item.Definition.RegistrationInfo.Description.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var splt in split)
                    {
                        string[] data = splt.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        switch (data[0])
                        {
                            case "active":
                                if (data[1] == "1")
                                {
                                    schedule.Active = 1;
                                }
                                break;
                            case "startHour":
                                int pstah;

                                if (int.TryParse(data[1], out pstah))
                                {
                                    if (pstah >= 0 || pstah < 24)
                                    {
                                        schedule.StartHour = pstah;
                                    }
                                }
                                break;
                            case "startMinute":
                                int pstam;

                                if (int.TryParse(data[1], out pstam))
                                {
                                    if (pstam >= 0 || pstam < 60)
                                    {
                                        schedule.StartMinute = pstam;
                                    }
                                }
                                break;
                            case "stopHour":
                                int pstoh;

                                if (int.TryParse(data[1], out pstoh))
                                {
                                    if (pstoh >= 0 || pstoh < 24)
                                    {
                                        schedule.StopHour = pstoh;
                                    }
                                }
                                break;
                            case "stopMinute":
                                int pstom;

                                if (int.TryParse(data[1], out pstom))
                                {
                                    if (pstom >= 0 || pstom < 60)
                                    {
                                        schedule.StopMinute = pstom;
                                    }
                                }
                                break;
                            case "shutdown":
                                if (data[1] == "1")
                                {
                                    schedule.Shutdown = 1;
                                }
                                break;
                            case "startmethod":
                                switch (data[1])
                                {
                                    case "100":
                                        startMethod = "100";
                                        break;
                                    case "010":
                                        startMethod = "010";
                                        break;
                                    default:
                                        startMethod = "001";
                                        break;
                                }
                                break;
                        }
                    }

                    if (item.Name.EndsWith("_Mon"))
                    {
                        schedule.Day = 0;
                    }
                    if (item.Name.EndsWith("_Tue"))
                    {
                        schedule.Day = 1;
                    }
                    if (item.Name.EndsWith("_Wed"))
                    {
                        schedule.Day = 2;
                    }
                    if (item.Name.EndsWith("_Thu"))
                    {
                        schedule.Day = 3;
                    }
                    if (item.Name.EndsWith("_Fri"))
                    {
                        schedule.Day = 4;
                    }
                    if (item.Name.EndsWith("_Sat"))
                    {
                        schedule.Day = 5;
                    }
                    if (item.Name.EndsWith("_Sun"))
                    {
                        schedule.Day = 6;
                    }

                    if (item.Enabled)
                    {
                        schedule.StartPC = 1;
                    }

                    // add info to return list
                    schedulesFound.Add(schedule);
                }
            }

            return schedulesFound;
        }
     }
}
