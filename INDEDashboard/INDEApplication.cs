﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    #region Enumerations
    
    /// <summary>
    /// Enumeration of possible main app starting methods
    /// </summary>
    public enum AppStartMethod
    {
        schedule,
        onstart,
        ondemand
    }

    /// <summary>
    /// Enumeration of app types
    /// </summary>
    public enum AppType
    {
        block,
        main,
        ondemand,
        alwayson,
        website,
        unknown
    }

    /// <summary>
    /// Indicates application run state
    /// </summary>
    public enum State
    {
        running,
        stopped,
        check,
    }

    #endregion
    
    /// <summary>
    /// Stores application data launches, checks and closes the defined application, manages the necessary UI features
    /// </summary>
    public class INDEApplication
    {
        #region Fields

        // the process
        private System.Diagnostics.Process process;

        //
        private string friendlyName;
        private State state;
        private State checkState;
        public int pid;
        private string pron;
        private bool closeByID;
        private bool uiVisible;
        private List<DailySchedule> schedule;
        private AppStartMethod startMethod;
        Timer updateTimer;
        private int cooldown;
        private bool scheduleOverride;
        private bool scheduledShutdown;
        private bool launchSeqRunning;

        private Control buttonParent;
        private Control scheduleUIParent;

        // App info label support
        AppInfoLabel infoLabel;

        #endregion

        #region Properties

        /// <summary>
        /// Stores the main app start method or the website URL
        /// </summary>
        public Control ScheduleUIParent
        {
            get
            {
                return scheduleUIParent;
            }
            set
            {
                scheduleUIParent = value;
            }
        }
        /// <summary>
        /// Stores the parent object for the app toggle button
        /// </summary>
        public Control ButtonParent
        {
            get
            {
                return buttonParent;
            }
            set
            {
                buttonParent = value;
            }
        }
        /// <summary>
        /// Stores the application start method. Default value: ondemand
        /// </summary>
        public AppStartMethod StartMethod
        {
            get
            {
                return startMethod;
            }
            set
            {
                startMethod = value;
            }
        }

        public string FilePath { get; set; }
        public AppType AppType { get; set; }
        public bool UseShellExecute { get; set; }
        public long FileSize { get; set; }
        public string AsUser { get; set; }
        public string FriendlyName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(friendlyName))
                {
                    return System.IO.Path.GetFileNameWithoutExtension(FilePath);
                }
                else
                {
                    return friendlyName;
                }
            }
            set { friendlyName = value; }
        }
        public string ScreenConfig
        {
            get; set;
        }
        public string Image
        {
            get; set;
        }
        public string Arguments
        {
            get; set;
        }
        public AppToggleButton appToggleButton
        {
            get; set;
        }
        ComboBox comboStartMethod;
        /// <summary>
        /// Gets or sets the visibility of the UI elements of the application
        /// </summary>
        public bool UIVisible
        {
            get
            {
                return uiVisible;
            }
            set
            {
                UDPDebug.Write.White(friendlyName + " SET " + value, false);

                if (AppType != AppType.alwayson && AppType != AppType.block)
                {
                    uiVisible = value;
                    appToggleButton.Button.Visible = value;
                    infoLabel.Label.Visible = value;
                }
            }
        }
        /// <summary>
        /// Sets if the app was closed because of a scheduled dashboard shutdown
        /// </summary>
        public bool ScheduledShutdown
        {
            set
            {
                scheduledShutdown = value;
            }
        }
        public EventHandler ShutdownSequenceStart;

        #endregion

        #region Constructors

        public INDEApplication()
        {
            infoLabel = new AppInfoLabel();

            startMethod = AppStartMethod.ondemand;
            friendlyName = string.Empty;
            AppType = AppType.unknown;
            UseShellExecute = false;
            state = State.stopped;
            AsUser = string.Empty;
            FileSize = -1;
            pid = -1;
            pron = string.Empty;
            closeByID = false;
            ScreenConfig = string.Empty;
            launchSeqRunning = false;
            cooldown = 0;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Initialises the process. Adds credentials if necessary. Has to be called before launching an event
        /// </summary>
        public void Initialize(bool startUp)
        {
            // close all possible instances
            string close = System.IO.Path.GetFileNameWithoutExtension(FilePath);

            if (!string.IsNullOrEmpty(Image))
            {
                close = Image;
            }

            if (startUp)
            {
                try
                {
                    foreach (var item in System.Diagnostics.Process.GetProcesses())
                    {
                        if (item.ProcessName.ToLower() == close.ToLower())
                        {
                            item.Kill();
                        }
                    }
                }
                catch (Exception)
                {
                }

                // draw buttons and UI elements, set initial cooldown timers 
                switch (AppType)
                {
                    case AppType.main:
                        if (scheduleUIParent != null && startUp)
                        {
                            DrawSchedulePanel();
                        }
                        if (buttonParent != null && startUp)
                        {
                            DrawButton();
                        }
                        break;
                    case AppType.ondemand:
                        if (buttonParent != null)
                        {
                            DrawButton();
                        }
                        break;
                    case AppType.website:
                        if (buttonParent != null)
                        {
                            DrawButton();
                        }
                        break;
                    case AppType.alwayson:
                        break;
                }
            }
            if (AppType != AppType.block)
            {
                try
                {
                    // configure process instances
                    process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = FilePath;
                    process.EnableRaisingEvents = true;

                    pid = -1;

                    if (!string.IsNullOrEmpty(AsUser))
                    {
                        foreach (var item in INI.credentials)
                        {
                            if (AsUser == item.Username)
                            {
                                process.StartInfo.UserName = item.Username;
                                process.StartInfo.UseShellExecute = UseShellExecute;
                                if (item.Password.Length > 0)
                                {
                                    process.StartInfo.Password = item.Password;
                                }
                                process.StartInfo.LoadUserProfile = true;
                            }
                        }
                    }

                    // check if file exists and file size matches
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.FileInfo info = new System.IO.FileInfo(FilePath);

                        if (FileSize != -1)
                        {
                            if (FileSize != info.Length)
                            {
                                FileSize = -666;
                                state = State.stopped;
                                infoLabel.AddInfo(new AppInfo(PresetMessage.BadFile));
                            }
                        }
                    }
                    else
                    {
                        state = State.stopped;
                        infoLabel.AddInfo(new AppInfo(PresetMessage.NoFile));
                    }

                    // assign arguments
                    if (!string.IsNullOrEmpty(Arguments))
                    {
                        process.StartInfo.Arguments = Arguments;
                    }

                    // set up process.exited event
                    process.Exited += Process_Exited;
                }
                catch (Exception ex)
                {
                    state = State.stopped;
                    infoLabel.AddInfo(new AppInfo(PresetMessage.NoFile));

                    UDPDebug.Write.White("APPLICATION - " + friendlyName + " UNABLE TO INITIALISE\r\n" + ex, false);
                }

                UDPDebug.Write.White("APPLICATION - " + friendlyName + " INITIALISED " + process.StartInfo.FileName, false);
            }

            // set up update timer
            updateTimer = new Timer();
            updateTimer.Interval = UIProperties.Application_UpdateInterval_MilliSec;

            if (AppType == AppType.block)
            {
                updateTimer.Interval = UIProperties.Application_UpdateInterval_MilliSec * 6;
                UDPDebug.Write.White("APPLICATION - " + friendlyName + " INITIALISED ", false);
            }

            updateTimer.Tick += UpdateTimer_Tick;
            updateTimer.Enabled = true;

            scheduledShutdown = false;
        }

        /// <summary>
        /// Draws the app button and calls info label creation
        /// </summary>
        /// <param name="buttonParent"></param>
        public void DrawButton()
        {
            if (AppType == AppType.main)
            {
                appToggleButton = new AppToggleButton(ButtonParent, friendlyName, UIProperties.Button_MainApp_Location, UIProperties.Button_Size_Main,
                    true, this.state);

                appToggleButton.Button.Click += AppButton_Click;
                DrawInfoLabel(buttonParent, new Point(appToggleButton.Button.Location.X + UIProperties.InfoLabel_Location_Offset.X, appToggleButton.Button.Location.Y + UIProperties.InfoLabel_Location_Offset.Y));
            }
            if (AppType == AppType.ondemand || AppType == AppType.website)
            {
                appToggleButton = new AppToggleButton(ButtonParent, friendlyName, UIProperties.Button_OnDemandApp_Location, UIProperties.Button_Size_Main,
                    true, this.state);
                appToggleButton.Button.Visible = false;
                appToggleButton.Button.Click += AppButton_Click;

                DrawInfoLabel(buttonParent, new Point(appToggleButton.Button.Location.X + UIProperties.InfoLabel_Location_Offset.X, appToggleButton.Button.Location.Y + UIProperties.InfoLabel_Location_Offset.Y));
            }
            if (AppType == AppType.alwayson)
            {
                appToggleButton = new AppToggleButton(ButtonParent, "", new Point(0, 0), new Size(0, 0),
                    false, this.state);
            }
        }

        /// <summary>
        /// Write app schedule in the Scheduled Task
        /// </summary>
        public void WriteSchedule()
        {
            string sm;

            switch (startMethod)
            {
                case AppStartMethod.schedule:
                    sm = "100";
                    break;
                case AppStartMethod.onstart:
                    sm = "010";
                    break;
                default:
                    sm = "001";
                    break;
            }

            if (schedule != null)
            {
                foreach (var item in schedule)
                {
                    WinTaskSched.CreateWeeklySchedule(friendlyName, item.Day, item.Active, item.StartHour, item.StartMinute,
                        item.StartPC, item.StopHour, item.StopMinute, item.Shutdown, sm);
                }
            }
        }

        /// <summary>
        /// Closes the running process
        /// </summary>
        public void Kill(bool onSchedule)
        {
            if (cooldown <= 0)
            {
                UDPDebug.Write.White("APPLICATION - " + friendlyName + " STOPPING ", false);

                cooldown = UIProperties.Button_Cooldown_Millisec;

                if (CheckAppState() == State.running)
                {
                    try
                    {
                        if (!closeByID)
                        {
                            process.CloseMainWindow();
                            state = State.stopped;
                        }
                        else
                        {
                            System.Diagnostics.Process.GetProcessById(pid).Kill();

                            state = State.stopped;
                        }
                    }
                    catch (Exception ex)
                    {
                        state = State.stopped;
                        infoLabel.AddInfo(new AppInfo(PresetMessage.KillError));

                        UDPDebug.Write.White("APPLICATION - " + friendlyName + " KILL ERROR\r\n" + ex, false);
                    }
                }
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Draws the Info label
        /// </summary>
        /// <param name="labelParent">Parent control on the UI</param>
        /// <param name="locationX">Horizontal location</param>
        /// <param name="locationY">Vertical location</param>
        private void DrawInfoLabel(Control labelParent, Point labelLocation)
        {
            infoLabel.AddLabel(labelParent, labelLocation, UIProperties.InfoLabel_Size, UIProperties.Font_Medium);
        }

        /// <summary>
        /// Draws the schedule control
        /// </summary>
        /// <param name="parent">Parent control on the UI</param>
        private void DrawSchedulePanel()
        {
            if (ScheduleUIParent != null)
            {
                // initialises the schedule tab page graphic elements and sdata storage
                schedule = new List<DailySchedule>(7);

                for (int i = 0; i < schedule.Capacity; i++)
                {
                    schedule.Add(new DailySchedule(i, ScheduleUIParent));
                }

                comboStartMethod = new ComboBox();
                comboStartMethod.BackColor = UIProperties.BackColour_Default;
                comboStartMethod.ForeColor = UIProperties.FontColor_Default;
                comboStartMethod.Size = UIProperties.ComboStartMethod_Size;
                comboStartMethod.Location = UIProperties.ComboStartMethod_Default_Location;
                comboStartMethod.Parent = ScheduleUIParent;
                comboStartMethod.Show();
                comboStartMethod.SelectedValueChanged += ComboStartMethod_SelectedValueChanged;

                foreach (var item in UIProperties.ComboSchStartMethodItems)
                {
                    comboStartMethod.Items.Add(item);
                }

                ReadSchedule();

                switch (startMethod)
                {
                    case AppStartMethod.schedule:
                        comboStartMethod.Text = "on schedule";
                        break;
                    case AppStartMethod.onstart:
                        comboStartMethod.Text = "when the computer starts";
                        break;
                    default:
                        comboStartMethod.Text = "on demand";
                        break;
                }
            }
        }

        /// <summary>
        /// Launches the application
        /// </summary>
        /// <returns></returns>
        private State Launch(bool scheduled)
        {
            UDPDebug.Write.White("APPLICATION - " + friendlyName + " LAUNCHING ", false);

            if (cooldown <= 0 && process != null)
            {
                launchSeqRunning = true;

                updateTimer.Enabled = true;

                // stacktrace for testing purposes only
                System.Diagnostics.StackTrace trac = new System.Diagnostics.StackTrace();

                if (state != State.running)
                {
                    if (FileSize != -666)
                    {
                        if (CheckScreenConfig())
                        {
                            try
                            {
                                process.Start();
                                state = State.running;
                                pid = process.Id;
                                pron = process.ProcessName;
                                cooldown = UIProperties.Button_Cooldown_Millisec;
                            }
                            catch (Exception ex)
                            {
                                state = State.stopped;
                                infoLabel.AddInfo(new AppInfo(PresetMessage.LaunchError));
                                cooldown = UIProperties.Button_Cooldown_Millisec;

                                UDPDebug.Write.White("APPLICATION - " + friendlyName + "LAUNCH ERROR\r\r" + ex, false);
                            }
                        }
                        else
                        {
                            state = State.stopped;
                            infoLabel.AddInfo(new AppInfo(PresetMessage.ScreenError));
                            cooldown = UIProperties.Button_Cooldown_Millisec;
                        }
                    }
                    else
                    {
                        state = State.stopped;
                        infoLabel.AddInfo(new AppInfo(PresetMessage.NoFile));
                        cooldown = UIProperties.Button_Cooldown_Millisec;
                    }
                }

                launchSeqRunning = false;
            }
            return state;
        }

        /// <summary>
        /// Checks the state of the application
        /// </summary>
        /// <returns></returns>
        private State CheckAppState()
        {
            checkState = State.check;

                if (System.IO.File.Exists(FilePath))
                {
                    checkState = State.stopped;

                    if (string.IsNullOrEmpty(Image))
                    {
                        foreach (var item in System.Diagnostics.Process.GetProcesses())
                        {
                            if (item.Id == pid)
                            {
                                checkState = State.running;
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in System.Diagnostics.Process.GetProcesses())
                        {
                            if (item.ProcessName == pron || item.ProcessName == Image)
                            {
                                process = System.Diagnostics.Process.GetProcessById(item.Id);

                                pid = item.Id;
                                pron = item.ProcessName;

                                closeByID = true;

                                checkState = State.running;
                            }
                        }
                    }
                }
                else
                {
                    checkState = State.stopped;
                    infoLabel.AddInfo(new AppInfo(PresetMessage.NoFile));
                }

                if (state == State.running)
                {
                    if (!CheckScreenConfig())
                    {
                        checkState = State.stopped;
                        infoLabel.AddInfo(new AppInfo(PresetMessage.ScreenError));
                    }
                }

            state = checkState;

            return state;
        }

        /// <summary>
        /// Checks whether or not the required screen config is available
        /// </summary>
        /// <returns>True means screen configuration available</returns>
        private bool CheckScreenConfig()
        {
            bool check = false;

            if (!string.IsNullOrEmpty(ScreenConfig))
            {
                foreach (var item in INI.screenConfig)
                {
                    Console.WriteLine("Checking sceen config " + ScreenConfig + " " + item.Name);
                    if (item.Name == ScreenConfig && item.Available())
                    {
                        check = true;
                    }
                }
            }
            else
            {
                check = true;
            }

            return check;
        }

        /// <summary>
        /// Check elements in the schedule list
        /// </summary>
        private State CheckSchedule()
        {
            DateTime now = DateTime.Now;
            State scheduledState = State.check;

            if (startMethod == AppStartMethod.schedule)
            {
                switch (now.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        {
                            if (schedule[0].Active == 1)
                            {
                                scheduledState = DailySchedule(0, now);
                            }
                        }
                        break;
                    case DayOfWeek.Tuesday:
                        {
                            if (schedule[1].Active == 1)
                            {
                                scheduledState = DailySchedule(1, now);
                            }
                        }
                        break;
                    case DayOfWeek.Wednesday:
                        {
                            if (schedule[2].Active == 1)
                            {
                                scheduledState = DailySchedule(2, now);
                            }
                        }
                        break;
                    case DayOfWeek.Thursday:
                        {
                            if (schedule[3].Active == 1)
                            {
                                scheduledState = DailySchedule(3, now);
                            }
                        }
                        break;
                    case DayOfWeek.Friday:
                        {
                            if (schedule[4].Active == 1)
                            {
                                scheduledState = DailySchedule(4, now);
                            }
                        }
                        break;
                    case DayOfWeek.Saturday:
                        {
                            if (schedule[5].Active == 1)
                            {
                                scheduledState = DailySchedule(5, now);
                            }
                        }
                        break;
                    case DayOfWeek.Sunday:
                        {
                            if (schedule[6].Active == 1)
                            {
                                scheduledState = DailySchedule(6, now);
                            }
                        }
                        break;
                }
            }

            return scheduledState;
        }

        /// <summary>
        /// Checks whether or not the application should run on the given day at the given time
        /// </summary>
        /// <param name="day"></param>
        /// <param name="now"></param>
        /// <returns></returns>
        private State DailySchedule(int day, DateTime now)
        {
            bool startPassed = false;
            bool stopPassed = false;
            bool startAndStopOrder = true;
            State scheduledState = State.check;

            // check whether start time passed
            if (schedule[day].StartHour != -1 && schedule[day].StartMinute != -1)
            {
                if (((now.Hour * 100) + now.Minute) >= ((schedule[day].StartHour * 100) + schedule[day].StartMinute))
                {
                    startPassed = true;
                }
            }

            // check whether stop time passed
            if (schedule[day].StopHour != -1 && schedule[day].StopMinute != -1 && (((now.Hour * 100) + now.Minute) >= schedule[day].StopHour * 100 + schedule[day].StopMinute))
            {
                stopPassed = true;
            }

            // check if start time is earlier than stop time
            if (schedule[day].StartHour != -1 && schedule[day].StartMinute != -1 && schedule[day].StopHour != -1 && schedule[day].StopMinute != -1)
            {
                if ((schedule[day].StopHour * 100 + schedule[day].StopMinute) < (schedule[day].StartHour * 100 + schedule[day].StartMinute))
                {
                    startAndStopOrder = false;
                }
            }

            if (startAndStopOrder)
            {
                if (startPassed && stopPassed)
                {
                    scheduledState = State.stopped;
                }
                else
                {
                    if (startPassed)
                    {
                        scheduledState = State.running;
                    }
                    else
                    {
                        scheduledState = State.stopped;
                    }
                }
            }
            else
            {
                if (startPassed && stopPassed)
                {
                    scheduledState = State.running;
                }
                else
                {
                    if (stopPassed)
                    {
                        scheduledState = State.stopped;
                    }
                    else
                    {
                        scheduledState = State.running;
                    }
                }
            }

            if (schedule[day].Shutdown == 1)
            {
                scheduledShutdown = true;
            }

            return scheduledState;
        }

        /// <summary>
        /// Executes the main application scheduled action
        /// </summary>
        /// <param name="scheduledState"></param>
        private void ScheduledAction(State scheduledState)
        {
            if (scheduledState != State.check && state != scheduledState)
            {
                if (state == State.running)
                {
                    Kill(true);

                    if (scheduledShutdown)
                    {
                        OnScheduledShutdownStart();
                    }
                }
                else
                {
                    Launch(true);
                }
            }
        }

        /// <summary>
        /// Reads the schedule file and fills the matching ini fields
        /// </summary>
        private void ReadSchedule()
        {
            if (schedule != null)
            {
                List<DailySchedule> readSchedule = WinTaskSched.GetWeeklySchedules(FriendlyName);

                foreach (var item in readSchedule)
                {
                    schedule[item.Day].Day = item.Day;
                    schedule[item.Day].Shutdown = item.Shutdown;
                    schedule[item.Day].StartHour = item.StartHour;
                    schedule[item.Day].StartMinute = item.StartMinute;
                    schedule[item.Day].StartPC = item.StartPC;
                    schedule[item.Day].StopHour = item.StopHour;
                    schedule[item.Day].StopMinute = item.StopMinute;
                    schedule[item.Day].Active = item.Active;
                }

                switch (WinTaskSched.startMethod)
                {
                    case "100":
                        startMethod = AppStartMethod.schedule;
                        break;
                    case "010":
                        startMethod = AppStartMethod.onstart;
                        break;
                    default:
                        startMethod = AppStartMethod.ondemand;
                        break;
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Launches/stops application on buttonclick and updates button info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AppButton_Click(object sender, EventArgs e)
        {
            if (AppType == AppType.ondemand)
            {
            }
            if (AppType == AppType.main)
            {
                if (startMethod == AppStartMethod.schedule)
                {
                    scheduleOverride = !scheduleOverride;
                }
                if (startMethod == AppStartMethod.onstart)
                {
                    scheduleOverride = true;
                }
            }

            if (state == State.running)
            {
                Kill(false);
            }
            else
            {
                Launch(false);
            }
        }

        /// <summary>
        /// Triggered at the app update timer tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            if (AppType != AppType.block)
            {
                if (cooldown > 0)
                {
                    cooldown = cooldown - UIProperties.Application_UpdateInterval_MilliSec;
                }

                CheckAppState();

                // check main app schedule and alwayson app state (relaunch if needed)
                switch (AppType)
                {
                    case AppType.main:
                        if (startMethod == AppStartMethod.schedule)
                        {
                            State scheduledState = CheckSchedule();

                            if (scheduledState != state)
                            {
                                if (!scheduleOverride)
                                {
                                    ScheduledAction(scheduledState);
                                }
                                else
                                {
                                    if (scheduledState != State.check)
                                    {
                                        infoLabel.AddInfo(new AppInfo(PresetMessage.OffSchedule));
                                    }
                                }
                            }

                            if (infoLabel.HighestErrorLevel == 2)
                            {
                                appToggleButton.ButtonEnabled = false;
                            }

                            if (cooldown > 0)
                            {
                                appToggleButton.ButtonEnabled = false;
                            }
                            else
                            {
                                appToggleButton.ButtonEnabled = true;
                            }
                        }
                        if (startMethod == AppStartMethod.onstart)
                        {
                            if (state != State.running)
                            {
                                if (!scheduledShutdown && !launchSeqRunning)
                                {
                                    Launch(false);
                                }
                            }
                        }

                        appToggleButton.ErrorLevel = infoLabel.HighestErrorLevel;
                        appToggleButton.AppState = state;
                        infoLabel.Update();

                        break;
                    case AppType.ondemand:

                        appToggleButton.ErrorLevel = infoLabel.HighestErrorLevel;
                        appToggleButton.AppState = state;
                        infoLabel.Update();

                        if (infoLabel.HighestErrorLevel == 2)
                        {
                            appToggleButton.ButtonEnabled = false;
                        }
                        break;
                    case AppType.website:

                        appToggleButton.ErrorLevel = infoLabel.HighestErrorLevel;
                        appToggleButton.AppState = state;
                        infoLabel.Update();

                        if (infoLabel.HighestErrorLevel == 2)
                        {
                            appToggleButton.ButtonEnabled = false;
                        }
                        break;
                    case AppType.alwayson:
                        if (state != State.running)
                        {
                            if (!scheduledShutdown && cooldown <= 0)
                            {
                                Initialize(true);
                                Launch(false);
                            }
                        }
                        break;
                }
            }
            else
            {
                UDPDebug.Write.White("BLOCK TICK", false);

                // kill 'block' processes
                foreach (var item in System.Diagnostics.Process.GetProcesses())
                {
                    if (item.ProcessName.ToLower().Contains(FilePath.ToLower()))
                    {
                        try
                        {
                            item.Kill();
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Triggered when an application has been terminated. Re-initialises the process object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Process_Exited(object sender, EventArgs e) 
        {
            cooldown = UIProperties.Button_Cooldown_Millisec;

            if (AppType == AppType.ondemand || AppType == AppType.main)
            {
                if (infoLabel.HighestErrorLevel == 0)
                {
                    appToggleButton.ErrorLevel = 0;
                    //appToggleButton.AppState = State.stopped;
                }
            }

            scheduleOverride = false;
            state = State.check;

            process.Close();

            UDPDebug.Write.White("APPLICATION - " + friendlyName + " EXITED ", false);
        }

        /// <summary>
        /// Triggered whenthe main app start method has been cahnged in the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboStartMethod_SelectedValueChanged(object sender, EventArgs e)
        {
            switch (comboStartMethod.SelectedItem.ToString())
            {
                case "on demand":
                    startMethod = AppStartMethod.ondemand;
                    break;
                case "when the computer starts":
                    startMethod = AppStartMethod.onstart;
                    break;
                case "on schedule":
                    startMethod = AppStartMethod.schedule;
                    break;
            }
        }

        /// <summary>
        /// Gets called when the schedule shuts down the PC
        /// </summary>
        protected virtual void OnScheduledShutdownStart()
        {
            if (ShutdownSequenceStart != null)
            {
                ShutdownSequenceStart(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}

