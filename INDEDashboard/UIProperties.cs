﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace INDEDashboard
{
    /// <summary>
    /// Contains UI elemnt drawing properties
    /// </summary>
    public static class UIProperties
    {
        // generic values
        public static int Application_UpdateInterval_MilliSec = 500; 
        public static int MajorError_Expiry_Interval_Ticks = 2000;
        public static int MinorError_Expiry_Interval_Ticks = 240;
        public static Color BackColour_Default = Color.White;
        public static int FontSize_Default;
        public static Color FontColor_Default = Color.Black;
        public static Font Font_Small = new Font("Microsoft Sans Serif", 8);
        public static Font Font_Medium = new Font("Microsoft Sans Serif", 10);
        public static Font Font_Large = new Font("Microsoft Sans Serif", 20);
        public static int App_Termination_Delay_Millisec = 60000;        

        // app button
        public static Point Button_MainApp_Location = new Point(162, 6);
        public static Point Button_OnDemandApp_Location = new Point(162, 87);
        public static Size Button_Size_Main = new Size(224, 41);
        public static Color Button_BackColor_MajorError = Color.Red;
        public static Color Button_BackColor_MinorError = Color.Yellow;
        public static Color Button_BackColor_Running = Color.Lime;
        public static Color Button_FontColor_Alternative = Color.White;
        public static FlatStyle Button_FlatStyle_Main = FlatStyle.Flat;
        public static int Button_BlinkInterval_Slow = 1500;
        public static int Button_BlinkInterval_Medium = 800;
        public static int Button_BlinkInterval_Fast = 500;
        public static int Button_Cooldown_Millisec = 10000;

        // info label
        public static Point InfoLabel_Location_Offset = new Point(-157, 41);
        public static Size InfoLabel_Size = new Size(420, 23);
        public static int InfoLabel_VisibleTextLength = 52;
        public static int InfoLabel_UpdateInterval_Ticks = 7;

        // IP tetxtbox
        public static Size IPTextbox_Item_Size = new Size(30, 22);

        // schedule panel
        static public List<string> ComboSchStartMethodItems 
            = new List<string> { "on demand", "when the computer starts", "on schedule" };
        static public Size ComboStartMethod_Size = new Size(152, 21);
        static public Point ComboStartMethod_Default_Location = new Point(77, 6);
        public static Size Schedule_Line_Panel_Size = new Size(300, 26);
        public static Point Schedule_Line_Panel_Location = new Point(75, 50);
    }
}
