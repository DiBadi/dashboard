﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Net;

namespace INDEDashboard
{
    class NetworkAdapters
    {
        NetworkInterface[] adapters;
        
        #region Properties

        public NetworkInterface[] Adapters
        {
            get
            {
                return adapters;
            }
            set
            {
                adapters = value;
            }
        }

        #endregion

        #region Constructors

        public NetworkAdapters()
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the network adapters
        /// </summary>
        /// <returns></returns>
        public NetworkInterface[] GetNetworkAdapters()
        {
            adapters = NetworkInterface.GetAllNetworkInterfaces();

            return adapters;
        }

        /// <summary>
        /// Retrieves the IP configuretion of the adapter
        /// </summary>
        /// <param name="adapterNname">Name fo the adapter</param>
        /// <param name="adapterIPConfig">List of strings containing the IP address, subnet mask and default gateway respectively. Empty means unconfigured.</param>
        /// <returns></returns>
        public bool IsDhcp(string adapterName, out bool isConnected, out string ipAddress, out string subnetMask, 
            out string defaultGateway, out bool autoDns, out string dns1, out string dns2)
        {
            bool dhcp = false;

            ipAddress = string.Empty;
            subnetMask = string.Empty;
            defaultGateway = string.Empty;

            isConnected = false;

            autoDns = false;
            dns1 = string.Empty;
            dns2 = string.Empty;

            foreach (var item in GetNetworkAdapters())
            {
                if (item.Name == adapterName)
                {
                    dhcp = item.GetIPProperties().GetIPv4Properties().IsDhcpEnabled;
                    // getting registry subkey
                    string adapterId = item.Id.TrimStart(new char[] { '{' });
                    adapterId = adapterId.TrimEnd(new char[] { '}' });
                    string subkey = @"SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces\" + item.Id;

                    // if DHCP is active
                    if (dhcp)
                    {
                        // retrieving IP
                        string ip = HKLMAccess.GetString(subkey, "DhcpIPAddress");

                        if (ip != null)
                        {
                            ipAddress = ip;
                        }

                        // retrieving subnet mask
                        string mask = HKLMAccess.GetString(subkey, "DhcpSubnetMask");

                        if (mask != null)
                        {
                            subnetMask = mask;
                        }

                        // retrueving default gateway
                        string[] gateways = HKLMAccess.GetStringArray(subkey, "DhcpDefaultGateway");

                        if (gateways != null)
                        {
                            defaultGateway = gateways[0];
                        }
                    }
                    else
                    {
                        // retrieving IP
                        string[] ips = HKLMAccess.GetStringArray(subkey, "IPAddress");

                        if (ips != null)
                        {
                            ipAddress = ips[0];
                        }

                        // retrieving subnet mask
                        string[] masks = HKLMAccess.GetStringArray(subkey, "SubnetMask");

                        if (masks != null)
                        {
                            subnetMask = masks[0];
                        }

                        // retrueving default gateway
                        string[] gateways = HKLMAccess.GetStringArray(subkey, "DefaultGateway");

                        if (gateways != null)
                        {
                            defaultGateway = gateways[0];
                        }
                    }

                    string getDns = HKLMAccess.GetString(subkey, "NameServer");

                    if (string.IsNullOrEmpty(getDns))
                    {
                        getDns = HKLMAccess.GetString(subkey, "DhcpNameServer");

                        if (!string.IsNullOrEmpty(getDns))
                        {
                            autoDns = true;
                        }
                    }

                    if (getDns != null)
                    {
                        string[] split = getDns.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        if (split.Length == 1)
                        {
                            dns1 = split[0];
                        }
                        if (split.Length > 1)
                        {
                            dns1 = split[0];
                            dns2 = split[1];
                        }
                    }
                }
            }

            return dhcp;
        }

        #endregion
    }
}
