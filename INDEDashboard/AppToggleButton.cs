﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    /// <summary>
    /// A button that can indicate app state
    /// </summary>
    public class AppToggleButton
    {
        #region Fields

        private string appName;
        private string buttonMessage;
        private Timer blinkTimer;
        private Timer cooldownTimer;
        private bool buttonVisible;
        private bool buttonEnabled;
        private int errorLevel;
        private State appState;

        #endregion

        #region Properties

        /// <summary>
        /// Enables/disables button
        /// </summary>
        public bool ButtonEnabled
        {
            get
            {
                return buttonEnabled;
            }
            set
            {
                buttonEnabled = value;
                Button.Enabled = value;
            }
        }

        /// <summary>
        /// Sets button visibility
        /// </summary>
        public bool ButtonVisible
        {
            set
            {
                buttonVisible = value;
                Button.Visible = value;
            }
        }

        /// <summary>
        /// Button control
        /// </summary>
        public Button Button
        {
            get; set;
        }

        /// <summary>
        /// Set error level for button visuals
        /// </summary>
        public int ErrorLevel
        {
            set
            {
                errorLevel = value;
            }
        }

        public State AppState
        {
            set
            {
                appState = value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new app button
        /// </summary>
        /// <param name="parent">Parent control of the button</param>
        /// <param name="appName">App name that gets displayed on the button</param>
        /// <param name="location">Button coordinates relative to the parent control</param>
        /// <param name="size">Button size</param>
        /// <param name="appState">App run state</param>
        public AppToggleButton(Control parent, string appName, Point location, Size size, bool visible, State appState)
        {
            Button = new Button();

            this.appName = appName.ToUpper();

            Button.Location = location;
            Button.Size = size;
            Button.BackColor = UIProperties.BackColour_Default;
            Button.ForeColor = UIProperties.FontColor_Default;
            Button.FlatStyle = UIProperties.Button_FlatStyle_Main;
            Button.Click += Button_Click;

            Button.Text = "START\r\n" + this.appName;

            this.appState = appState;

            blinkTimer = new Timer();
            blinkTimer.Interval = UIProperties.Button_BlinkInterval_Fast;
            blinkTimer.Tick += BlinkTimer_Tick;
            blinkTimer.Enabled = true;

            cooldownTimer = new Timer();
            cooldownTimer.Interval = UIProperties.Button_Cooldown_Millisec;
            cooldownTimer.Tick += CooldownTimer_Tick;
            cooldownTimer.Enabled = true;

            Button.Parent = parent;

            if (visible)
            {
                Button.Show();
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the button visuals
        /// </summary>
        /// <param name="cooldown">True disables the button</param>
        /// <param name="appButtonState">Sets the button visuals</param>
        /// <param name="message">Updates the button text. Null does not change the default button text</param>
        public void UpdateButton(int errorLevel, State appState, string message)
        {
            this.appState = appState;
            this.errorLevel = errorLevel;

            buttonMessage = message;

            // update button text
            if (message == null)
            {
                if (appState == State.running)
                {
                    Button.Text = "STOP\r\n" + appName;
                }
                else
                {
                    Button.Text = "START\r\n" + appName;
                }
            }
            else
            {
                Button.Text = message;
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Selects the button visuals based on the app state
        /// </summary>
        private void ChooseButtonColor()
        {
            switch (errorLevel)
            {
                case 0:
                    if (appState == State.running)
                    {
                        GreenBlinking();
                    }
                    else
                    {
                        NoBlinking();
                    }
                    break;
                case 1:
                    YellowBlinking();
                    break;
                case 2:
                    RedBlinking_Fast();
                    break;
            }
        }

        /// <summary>
        /// Updates button text based on the state of the app
        /// </summary>
        private void SetButtonText()
        {
            if (appState == State.running)
            {
                Button.Text = "STOP\r\n" + appName;
            }
            else
            {
                Button.Text = "START\r\n" + appName;
            }
        }

        /// <summary>
        /// Changes button viduals to blinking quickly with red colour
        /// </summary>
        private void RedBlinking_Fast()
        {
            blinkTimer.Enabled = true;

            blinkTimer.Interval = UIProperties.Button_BlinkInterval_Fast;
            if (Button.BackColor != UIProperties.Button_BackColor_MajorError)
            {
                Button.BackColor = UIProperties.Button_BackColor_MajorError;
                Button.ForeColor = UIProperties.Button_FontColor_Alternative;
            }
            else
            {
                Button.BackColor = UIProperties.BackColour_Default;
                Button.ForeColor = UIProperties.FontColor_Default;
            }
        }

        /// <summary>
        /// Changes button viduals to blinking slowly with red colour
        /// </summary>
        private void RedBlinking_Slow()
        {
            blinkTimer.Enabled = true;

            blinkTimer.Interval = UIProperties.Button_BlinkInterval_Medium;
            if (Button.BackColor != UIProperties.Button_BackColor_MajorError)
            {
                Button.BackColor = UIProperties.Button_BackColor_MajorError;
                Button.ForeColor = UIProperties.Button_FontColor_Alternative;
            }
            else
            {
                Button.BackColor = UIProperties.BackColour_Default;
                Button.ForeColor = UIProperties.FontColor_Default;
            }
        }

        /// <summary>
        /// Changes button viduals to blinking with yellow colour
        /// </summary>
        private void YellowBlinking()
        {
            blinkTimer.Enabled = true;

            blinkTimer.Interval = UIProperties.Button_BlinkInterval_Medium;
            if (Button.BackColor != UIProperties.Button_BackColor_MinorError)
            {
                Button.BackColor = UIProperties.Button_BackColor_MinorError;
            }
            else
            {
                Button.BackColor = UIProperties.BackColour_Default;
                Button.ForeColor = UIProperties.FontColor_Default;
            }
        }

        /// <summary>
        /// Changes button viduals to blinking with green colour
        /// </summary>
        private void GreenBlinking()
        {
            if (!blinkTimer.Enabled)
            {
                blinkTimer.Enabled = true;
            }

            if (blinkTimer.Interval != UIProperties.Button_BlinkInterval_Slow)
            {
                blinkTimer.Interval = UIProperties.Button_BlinkInterval_Slow;
            }

            if (Button.BackColor != UIProperties.Button_BackColor_Running)
            {
                Button.BackColor = UIProperties.Button_BackColor_Running;
                //Button.ForeColor = UIProperties.Button_FontColor_Alternative;
            }
            else
            {
                Button.BackColor = UIProperties.BackColour_Default;
                Button.ForeColor = UIProperties.FontColor_Default;
            }
        }

        /// <summary>
        /// Static default button backround
        /// </summary>
        private void NoBlinking()
        {
            Button.ForeColor = UIProperties.FontColor_Default;
            Button.BackColor = UIProperties.BackColour_Default;
        }

        #endregion

        #region Events

        /// <summary>
        /// Disables the button for the cooldown period and enables the cooldown timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, EventArgs e)
        {
            cooldownTimer.Enabled = true;
            Button.Enabled = false;
        }

        /// <summary>
        /// Gets called at cooldown timer tick. Disables the timer and enable the button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CooldownTimer_Tick(object sender, EventArgs e)
        {
            cooldownTimer.Enabled = false;
            Button.Enabled = true;

            if (cooldownTimer.Interval == UIProperties.Button_Cooldown_Millisec)
            {
                cooldownTimer.Interval = UIProperties.Button_Cooldown_Millisec;
            }
        }

        /// <summary>
        /// Button blink timer tick. Triggers button color change based on the app state and error level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BlinkTimer_Tick(object sender, EventArgs e)
        {
            ChooseButtonColor();
            SetButtonText();
        }

        #endregion
    }
}
