﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace INDEDashboard
{
    /// <summary>
    /// Loads, stores and saves all necessary data of the handled application and users and working conditions 
    /// </summary>
    public class INI
    {
        #region Fields

        // Data read-write support
        Crypto crypto;

        // File paths
        string publicINIPath = @"c:\tools\dashboard.ini";
        string readINIPath = @"c:\tools\dashboard.read";

        // Loaded data storage
        private List<INDEApplication> applications = new List<INDEApplication>();
        public static List<AsUserData> credentials = new List<AsUserData>();
        public static List<ScreenConfig> screenConfig = new List<ScreenConfig>();
        public static string dashboardUser;
        public static bool switchOff;

        #endregion

        #region Properties

        public List<INDEApplication> Applications
        {
            get
            {
                return applications;
            }
            set
            {
                applications = value;
            }
        }

        #endregion

        #region Constructors

        public INI()
        {
            dashboardUser = string.Empty;
            switchOff = false;

            // initialises the cryptographic services
            crypto = new Crypto(HKCUAccess.GetStringValue(@"SOFTWARE\INDE\Dashboard", "StartInfo1"), 
                Encoding.Unicode.GetBytes(HKCUAccess.GetStringValue(@"SOFTWARE\INDE\Dashboard", "StartInfo2")));
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Starts the setup loading sequence from either public or encrypted files. Writes ini file if read file is found
        /// </summary>
        public void LoadINI(bool deletePublicINIFile)
        {
            // check public ini file
            if (File.Exists(publicINIPath))
            {
                ReadPublicINI();

                // write as user data to the registry
                if (credentials.Count > 0)
                {
                    WriteAsUserData();
                }

                // write app data to the encrypted ini file
                if (applications.Count > 0)
                {
                    WriteAppData();
                }

                if (deletePublicINIFile)
                {
                    try
                    {
                        File.Delete(publicINIPath);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else
            {
                // extract application data from encrypted ini file and load to applications List
                ReadAppData();

                // extract asuser data from registry and load it into asuser List
                ReadAsUserData();
            }

            if (File.Exists(readINIPath))
            {
                // save config file if READ file found
                try
                {
                    File.WriteAllText(publicINIPath, GetSaveINI());
                    if (deletePublicINIFile)
                    {
                        File.Delete(readINIPath);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Read public ini file and sort application, dashboard user, credential, website items into the matching container
        /// </summary>
        private void ReadPublicINI()
        {
            try
            {
                StreamReader reader = new StreamReader(publicINIPath);

                while (reader.Peek() != -1)
                {
                    string readLine = reader.ReadLine() + "\r\n";

                    if (!readLine.Contains("***"))
                    {
                        readLine = readLine.TrimEnd('\r', '\n');

                        // recovers suspend mode
                        if (readLine.StartsWith("suspend="))
                        {
                            ParseSuspendmode(readLine);
                        }

                        // recover application details
                        if (readLine.StartsWith("application="))
                        {
                            ParseAppData(readLine);
                        }

                        // recover user profile details
                        if (readLine.StartsWith("userprofile="))
                        {
                            ParseUserProfile(readLine);
                        }

                        // recover screenconfig details
                        if (readLine.StartsWith("screenconfig="))
                        {
                            ParseScreenConfig(readLine);
                        }

                        // recover dashboarduser
                        if (readLine.StartsWith("dashboarduser="))
                        {
                            ParseDashboardUser(readLine);
                        }

                        // recover website details
                        if (readLine.StartsWith("website="))
                        {
                            ParseWebsite(readLine, true);
                        }
                    }
                }

                reader.Dispose();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Clears existing as user list and loads data from the registry
        /// </summary>
        private void ReadAsUserData()
        {
            credentials.Clear();

            foreach (var item in HKCUAccess.GetValueNames(@"SOFTWARE\INDE\Dashboard\Data"))
            {
                string[] data = Encoding.Unicode.GetString(crypto.AES_Decrypt(HKCUAccess.GetByteArrayValue(@"SOFTWARE\INDE\Dashboard\Data", item))).Split(new string[] { "<...>" }, StringSplitOptions.None);

                ParseUserProfile("userprofile=" + data[1] + "|password=" + data[2]);
            }
        }

        /// <summary>
        /// Write as user data to the registry
        /// </summary>
        private void WriteAsUserData()
        {
            // clear existing as user data
            foreach (var item in HKCUAccess.GetValueNames(@"SOFTWARE\INDE\Dashboard\Data"))
            {
                HKCUAccess.DeleteValue(@"SOFTWARE\INDE\Dashboard\Data", item);
            }

            // write as user items to the registry
            foreach (var item in credentials)
            {
                string valueName = string.Empty;
                Random rnd = new Random();

                for (int i = 0; i < rnd.Next(10, 60); i++)
                {
                    int r = rnd.Next(0, 15);

                    if (r < 10)
                    {
                        valueName += r.ToString();
                    }
                    else
                    {
                        switch (r)
                        {
                            case 10:
                                valueName += "a";
                                break;
                            case 11:
                                valueName += "b";
                                break;
                            case 12:
                                valueName += "c";
                                break;
                            case 13:
                                valueName += "d";
                                break;
                            case 14:
                                valueName += "e";
                                break;
                            case 15:
                                valueName += "f";
                                break;
                            default:
                                break;
                        }
                    }
                }

                HKCUAccess.SetByteArrayValue(@"SOFTWARE\INDE\Dashboard\Data", valueName, 
                    crypto.AES_Encrypt(Encoding.Unicode.GetBytes(valueName + "<...>" + item.Username + 
                    "<...>" + item.Pw + "<...>" + valueName)));
            }
        }

        /// <summary>
        /// Reads application data from the registry
        /// </summary>
        private void ReadAppData()
        {
            applications.Clear();

            try
            {
                // reads the registry value
                byte[] fromIni = HKCUAccess.GetByteArrayValue(@"SOFTWARE\INDE\Dashboard", "StartInfo3");

                string iniString = Encoding.Unicode.GetString(crypto.AES_Decrypt(fromIni));

                StringReader reader = new StringReader(iniString);

                while (reader.Peek() != -1)
                {
                    string readLine = reader.ReadLine() + "\r\n";

                    if (!readLine.Contains("***"))
                    {
                        readLine = readLine.TrimEnd('\r', '\n');

                        // recovers suspend mode
                        if (readLine.StartsWith("suspend="))
                        {
                            ParseSuspendmode(readLine);
                        }

                        // recovers application data
                        if (readLine.StartsWith("application="))
                        {
                            ParseAppData(readLine);
                        }

                        // recovers the dashboard user level
                        if (readLine.StartsWith("dashboarduser="))
                        {
                            ParseDashboardUser(readLine);
                        }

                        // recover screenconfig details
                        if (readLine.StartsWith("screenconfig="))
                        {
                            ParseScreenConfig(readLine);
                        }

                        // recover website details
                        if (readLine.StartsWith("website="))
                        {
                            ParseWebsite(readLine, false);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Write app data to the registry
        /// </summary>
        private void WriteAppData()
        {
            byte[] cypherThis = Encoding.Unicode.GetBytes(GetSaveINI());
            byte[] saveThis = crypto.AES_Encrypt(cypherThis);

            HKCUAccess.SetByteArrayValue(@"SOFTWARE\INDE\Dashboard", "StartInfo3", saveThis);
        }

        /// <summary>
        /// Composes ini file to be saved
        /// </summary>
        /// <returns></returns>
        private string GetSaveINI()
        {
            string saveINI = "***       INDE Dashboard Configuration";
            saveINI += "\r\n***";
            saveINI += "\r\n";
            saveINI += "*** Define application as follows:";
            saveINI += "\r\n";
            saveINI += "*** application=<path>|type=<main/ondemand/alwayson/block>|name=<friendly name>|asuser=<user profile name>|size=<file size in bytes>|screen=<screen config friendly name>|image=<process name>|arguments=<start oarameters>";
            saveINI += "\r\n";
            saveINI += "*** 'main' application can be controlled from the UI main tab and schedule tab.";
            saveINI += "\r\n";
            saveINI += "*** Define only one 'main' application.";
            saveINI += "\r\n";
            saveINI += "*** 'ondemand' applications can be launched and stopped from the UI main tab.";
            saveINI += "\r\n";
            saveINI += "*** Define as many 'ondemand' applications as you want.";
            saveINI += "\r\n";
            saveINI += "*** 'alwayson' applications will be launched when the dashboard starts an will be automatically re-launched if not running.";
            saveINI += "\r\n";
            saveINI += "*** Define as many 'alwayson' applications as you want.";
            saveINI += "\r\n";
            saveINI += "*** 'block' applications will not be able to run.";
            saveINI += "\r\n";
            saveINI += "*** Define 'block' applications as shown:";
            saveINI += "\r\n";
            saveINI += "*** application=<process image name>|type=block.";
            saveINI += "\r\n";
            saveINI += "*** No other parameter is required for 'block' apps.";
            saveINI += "\r\n";
            saveINI += "*** Friendly name will be displayed on the UI.";
            saveINI += "\r\n";
            saveINI += "*** 'asuser' needed if the application needs to be launched as a different user.";
            saveINI += "\r\n";
            saveINI += "*** If 'size' is provided, filesize will be checked at launch.";
            saveINI += "\r\n";
            saveINI += "*** If 'image' is provided application will be searched by this name among processes.";
            saveINI += "\r\n";
            saveINI += "*** Define user profile information as shown below:";
            saveINI += "\r\n";
            saveINI += "*** Any new user profile data will overwrite all existing user profiles";
            saveINI += "\r\n";
            saveINI += "*** userprofile=<user profile name>|password=<password>";
            saveINI += "\r\n";
            saveINI += "*** Select dashboard user as shown:";
            saveINI += "\r\n";
            saveINI += "*** dashboarduser=<user profile name>";
            saveINI += "\r\n";
            saveINI += "*** Select a user profile that has got administrator privileges.";
            saveINI += "\r\n";
            saveINI += "*** Configure suspend mode:";
            saveINI += "\r\n";
            saveINI += "*** Switch off or put the pc to sleep if stop PC is selected in the schedule. (default: sleep)";
            saveINI += "\r\n";
            saveINI += "*** suspend=<sleep/off>";
            saveINI += "\r\n";
            saveINI += "*** Define screen configuration profile as shown:";
            saveINI += "\r\n";
            saveINI += "*** screenconfig=<screen config profile name>|primary=<main screen h x main screen v>|secondary=<secondary screen h x secondary screen v>|output=<program output screen>";
            saveINI += "\r\n";
            saveINI += "*** Websites will be opened in kiosk mode. Define websites as shown:";
            saveINI += "\r\n";
            saveINI += "*** website=<URL>|name=<display name - obligatory>";
            saveINI += "\r\n";
            saveINI += "\r\n***";
            saveINI += "\r\n*** SUSPEND MODE";
            saveINI += "\r\n***";

            if (switchOff)
            {
                saveINI += "\r\n";
                saveINI += "\r\n";
                saveINI += "suspend=off";
                saveINI += "\r\n";
            }
            else
            {
                saveINI += "\r\n";
                saveINI += "\r\n";
                saveINI += "suspend=sleep";
                saveINI += "\r\n";
            }

            if (!string.IsNullOrEmpty(dashboardUser))
            {
                saveINI += "\r\n***";
                saveINI += "\r\n*** DASHBOARD USER PROFILE";
                saveINI += "\r\n***";
                saveINI += "\r\n";
                saveINI += "\r\ndashboarduser=" + dashboardUser;
            }

            int websites = 0;
            string websiteINI = string.Empty;

            if (applications.Count > 0)
            {
                saveINI += "\r\n";
                saveINI += "\r\n***";
                saveINI += "\r\n*** APPLICATION INFO";
                saveINI += "\r\n***";
                saveINI += "\r\n";

                foreach (var item in applications)
                {
                    if (item.AppType != AppType.website)
                    {
                        saveINI += "\r\n";
                        saveINI += "application=" + item.FilePath + "|type=" + item.AppType;

                        if (!string.IsNullOrEmpty(item.FriendlyName))
                        {
                            saveINI += "|name=" + item.FriendlyName;
                        }
                        if (!string.IsNullOrEmpty(item.AsUser))
                        {
                            saveINI += "|asuser=" + item.AsUser;
                        }
                        if (!string.IsNullOrEmpty(item.ScreenConfig))
                        {
                            saveINI += "|screen=" + item.ScreenConfig;
                        }
                        if (!string.IsNullOrEmpty(item.Image))
                        {
                            saveINI += "|image=" + item.Image;
                        }
                        if (!string.IsNullOrEmpty(item.Arguments))
                        {
                            saveINI += "|image=" + item.Arguments;
                        }

                        saveINI += "\r\n";
                    }
                    else
                    {
                        websites++;

                        websiteINI += "\r\nwebsite=" + item.Arguments.Remove(item.Arguments.IndexOf(' ')) + "|name=" + item.FriendlyName.Remove(0, 5) + "\r\n"; 
                    }
                }

                if (websites > 0)
                {
                    saveINI += "\r\n***";
                    saveINI += "\r\n*** WEBSITES";
                    saveINI += "\r\n***";
                    saveINI += "\r\n";
                    saveINI += websiteINI;
                }
            }

            if (screenConfig.Count > 0)
            {
                saveINI += "\r\n***";
                saveINI += "\r\n*** SCREEN CONFIGURATIONS";
                saveINI += "\r\n***";

                foreach (var item in screenConfig)
                {
                    saveINI += "\r\nscreenconfig=" + item.Name;

                    if (item.PrimaryWidth != -1 && item.PrimaryHeight != -1)
                    {
                        saveINI += "|primary=" + item.PrimaryWidth + "x" + item.PrimaryHeight;
                    }
                    if (item.PrimaryWidth != -1 && item.PrimaryHeight != -1)
                    {
                        saveINI += "|secondary=" + item.SecondaryWidth + "x" + item.SecondaryHeight;
                    }

                    saveINI += "|output=" + item.Output;
                }
            }

            return saveINI;
        }

        /// <summary>
        /// Parses suspendmode from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        private void ParseSuspendmode(string line)
        {
            if (line.Contains("suspend=off"))
            {
                switchOff = true;
            }
            else
            {
                switchOff = false;    
            }

            UDPDebug.Write.White("INI - SWITCH OFF MODE " + switchOff + " true = switch off", false);
        }

        /// <summary>
        /// Parses application data from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        private void ParseAppData(string line)
        {
            INDEApplication application = new INDEApplication();

            string[] splitLine = line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in splitLine)
            {
                string[] split = item.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                if (item.Contains("application="))
                {
                    application.FilePath = split[1];
                }
                if (item.Contains("type="))
                {
                    switch (split[1])
                    {
                        case "main":
                            application.AppType = AppType.main;
                            break;
                        case "alwayson":
                            application.AppType = AppType.alwayson;
                            break;
                        case "ondemand":
                            application.AppType = AppType.ondemand;
                            break;
                        case "block":
                            application.AppType = AppType.block;
                            break;
                    }
                }
                if (item.Contains("name="))
                {
                    application.FriendlyName = split[1];
                }
                if (item.Contains("asuser="))
                {
                    application.AsUser = split[1];
                }
                if (item.Contains("screenconfig="))
                {
                    application.ScreenConfig = split[1];
                }
                if (item.Contains("size="))
                {
                    long prs;

                    if (long.TryParse(split[1], out prs))
                    {
                        application.FileSize = prs;
                    }
                }
                if (item.Contains("image="))
                {
                    application.Image = split[1];
                }
                if (item.Contains("arguments="))
                {
                    application.Arguments = split[1];
                }
                if (item.Contains("screen="))
                {
                    application.ScreenConfig = split[1];
                }
            }

            if (application.AppType != AppType.unknown && !string.IsNullOrEmpty(application.FilePath))
            {
                bool exists = false;

                foreach (var item in applications)
                {
                    if (application.AppType == AppType.main && item.AppType == AppType.main)
                    {
                        exists = true;
                    }

                    if (application.FriendlyName == item.FriendlyName)
                    {
                        exists = true;
                    }
                }

                if (!exists)
                {
                    applications.Add(application);

                    UDPDebug.Write.White("INI - APPLICATION ADDED " + application.FriendlyName + " (" + application.FilePath + ") " + "type:" + application.AppType + " as user " + application.AsUser, false);
                }
            }
        }

        /// <summary>
        /// Parses user profile data from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        private void ParseUserProfile(string line)
        {
            AsUserData asUserData = new AsUserData();

            string[] splitLine = line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in splitLine)
            {
                string[] splt = item.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                if (item.Contains("userprofile="))
                {
                    asUserData.Username = splt[1];
                }
                if (splt.Length > 1)
                {
                    if (item.Contains("password="))
                    {
                        asUserData.Pw = splt[1];
                    }
                }
            }

            if (!string.IsNullOrEmpty(asUserData.Username))
            {
                bool exists = false;

                foreach (var item in credentials)
                {
                    if (item.Username == asUserData.Username)
                    {
                        exists = true;
                    }
                }

                if (!exists)
                {
                    credentials.Add(asUserData);

                    UDPDebug.Write.White("INI - USER PROFILE ADDED " + asUserData.Username, false);
                }
            }
        }

        /// <summary>
        /// Parses screen config item from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        private void ParseScreenConfig(string line)
        {
            ScreenConfig screen = new ScreenConfig();

            string[] splitLine = line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in splitLine)
            {
                string[] splt = item.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                if (item.StartsWith("screenconfig="))
                {
                    screen.Name = splt[1];
                }
                if (item.StartsWith("primary="))
                {
                    string[] resolution = splt[1].Split(new char[] { 'x' }, StringSplitOptions.RemoveEmptyEntries);

                    int v;
                    int h;

                    if (int.TryParse(resolution[0], out h))
                    {
                        screen.PrimaryWidth = h;
                    }
                    if (int.TryParse(resolution[1], out v))
                    {
                        screen.PrimaryHeight = v;
                    }
                }
                if (item.StartsWith("secondary="))
                {
                    string[] resolution = splt[1].Split(new char[] { 'x' }, StringSplitOptions.RemoveEmptyEntries);

                    int v;
                    int h;

                    if (int.TryParse(resolution[0], out h))
                    {
                        screen.SecondaryWidth = h;
                    }
                    if (int.TryParse(resolution[1], out v))
                    {
                        screen.SecondaryHeight = v;
                    }
                }
                if (item.StartsWith("output="))
                {
                    if (splt[1].ToLower() == "primary")
                    {
                        screen.Output = OutputScreen.primary;
                    }
                    if (splt[1].ToLower() == "secondary")
                    {
                        screen.Output = OutputScreen.secondary;
                    }
                }
            }

            if (!string.IsNullOrEmpty(screen.Name))
            {
                bool exists = false;

                foreach (var item in screenConfig)
                {
                    if (item.Name == screen.Name)
                    {
                        exists = true;
                    }
                }

                if (!exists)
                {
                    screenConfig.Add(screen);

                    UDPDebug.Write.White("INI - SCREEN CONFIG ADDED " + screen.Name + " " + screen.Output, false);
                }
            }
        }

        /// <summary>
        /// Parses the dashboard user from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        private void ParseDashboardUser(string line)
        {
            string[] split = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

            if (split.Length > 1)
            {
                dashboardUser = split[1];
            }
        }

        /// <summary>
        /// Parses websites from the ini file
        /// </summary>
        /// <param name="line">One line from the ini file</param>
        /// <param name="fromPublic">True if the line string is from a public file</param>
        private void ParseWebsite(string line, bool fromPublic)
        {
            INDEApplication application = new INDEApplication();
            application.AppType = AppType.website;

            string[] splitLine = line.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in splitLine)
            {
                string[] splt = item.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                if (item.StartsWith("website=") && splt[1] != null)
                {
                    application.FilePath = Path.Combine(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), @"Browser\INDEBrowser.exe");

                    application.Arguments = "url=" + splt[1] + " timeout=10";
                }

                if (item.StartsWith("name=") && splt[1] != null)
                {
                    application.FriendlyName = splt[1];
                }

                if (!string.IsNullOrEmpty(application.FilePath) && !string.IsNullOrEmpty(application.FriendlyName))
                {
                    if (fromPublic)
                    {
                        application.FriendlyName = "web: " + application.FriendlyName;
                    }

                    bool exists = false;

                    foreach (var app in applications)
                    {
                        if (app.FriendlyName == application.FriendlyName)
                        {
                            exists = true;
                        }
                    }

                    if (!exists)
                    {
                        application.AppType = AppType.website;
                        applications.Add(application);

                        UDPDebug.Write.White("INI - WEBSITE ADDED " + application.Arguments, false);
                    }
                }
            }
        }

        #endregion
    }
}
