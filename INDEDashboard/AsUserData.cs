﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INDEDashboard
{
    /// <summary>
    /// Stores user data
    /// </summary>
    public class AsUserData
    {
        #region Fields

        private string pw;

        #endregion

        #region Properties

        public string Pw
        {
            get
            {
                string t = pw;
                pw = null;
                return t;
            }
            set
            {
                AddPassword(value);
                pw = value;
            }
        }
        public string Username { get; set; }
        public System.Security.SecureString Password { get; set; }

        #endregion

        #region Constructors

        public AsUserData()
        {
            Username = string.Empty;
            Password = null;
            pw = string.Empty;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Translates a string to a secure string
        /// </summary>
        /// <param name="pw"></param>
        private void AddPassword(string pw)
        {
            Password = new System.Security.SecureString();

            System.Security.SecureString s = new System.Security.SecureString();

            foreach (var c in pw)
            {
                s.AppendChar(c);
            }

            Password = s;

            this.pw = pw;
        }

        #endregion
    }
}
