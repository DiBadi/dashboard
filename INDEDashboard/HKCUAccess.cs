﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace INDEDashboard
{
    /// <summary>
    /// Accesses the registry HKEY_CURRENT_USER node
    /// </summary>
    static public class HKCUAccess
    {
        static public void SetStringValue(string subkey, string valueName, string value)
        {
            try
            {
                Registry.SetValue(@"HKEY_CURRENT_USER\" + subkey, valueName, value);
            }
            catch (Exception)
            {
            }
        }

        static public string GetStringValue(string subkey, string valueName)
        {
            try
            {
                return (string)Registry.GetValue(@"HKEY_CURRENT_USER\" + subkey, valueName, null);
            }
            catch (Exception)
            {
                return null;
            }
        }

        static public void SetByteArrayValue(string subkey, string valueName, byte[] value)
        {
            try
            {
                Registry.SetValue(@"HKEY_CURRENT_USER\" + subkey, valueName, value);
            }
            catch (Exception)
            {
            }
        }

        static public byte[] GetByteArrayValue(string subkey, string valueName)
        {
            try
            {
                return (byte[])Registry.GetValue(@"HKEY_CURRENT_USER\" + subkey, valueName, null);
            }
            catch (Exception)
            {
                return null;
            }
        }

        static public void DeleteValue(string subKey, string valueName)
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(subKey, true);

            try
            {
                regKey.DeleteValue(valueName);
            }
            catch (Exception)
            {
            }
        }

        static public void DeleteSubkeyTree(string subkey)
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(subkey, true);

            try
            {
                regKey.DeleteSubKeyTree(subkey);
            }
            catch (Exception)
            {
            }
            finally
            {
                regKey.Dispose();
            }
        }

        /// <summary>
        /// Gets subkey names inside a given key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        static public string[] GetSubkeys(string key)
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(key);
            return regKey.GetSubKeyNames();
        }

        /// <summary>
        /// Gets a list of value names in a given subkey
        /// </summary>
        /// <param name="subKey"></param>
        /// <returns></returns>
        static public List<string> GetValueNames(string subKey)
        {
            List<string> names = new List<string>();

            try
            {
                using (RegistryKey regKey = Registry.CurrentUser.OpenSubKey(subKey))
                {
                    foreach (var item in regKey.GetValueNames())
                    {
                        names.Add(item);
                    }
                }

            }
            catch (Exception)
            {
            }

            return names;
        }
    }
}
