﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace INDEDashboard
{
    public class HourOrMInuteBox
    {
        public int TextBoxValue { get; set; }

        public HourOrMInuteBox()
        {
            TextBoxValue = -1;
        }

        public TextBox GetTextBox(int hourOrMinuteField, Panel parent, int minValue, int maxValue, int locationX, int locationY)
        {
            TextBox hourOrMinuteBox = new TextBox();

            hourOrMinuteBox.TextChanged += HourOrMinuteBox_TextChanged;

            return hourOrMinuteBox;
        }

        private void HourOrMinuteBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
